//========================================================================
//
// Filename:     AdultMembership.java
//
// Description:  Simple version of the AdultMembership concrete class
//
//========================================================================


public class AdultMembership implements MembershipType {

	// Constructor
	// ---------------------------------------------------------------
	public AdultMembership() {

		// Nothing to do!
	}


	// Implement the methods from the MembershipType interface
	// ---------------------------------------------------------------
        public int maximumBorrowable() {
	
       		return booksPerAdult;
	}

	
	public boolean areFinesApplicable() {

		return true;
	}
        

	// Private class contants
	// ---------------------------------------------------------------
	private static final int booksPerAdult = 6;
}


