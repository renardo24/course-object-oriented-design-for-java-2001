//========================================================================
//
// Filename:     Member.java
//
// Description:  Simple version of the Member class
//
//========================================================================

import java.util.*;

public class Member {

	// Constructor
	// ---------------------------------------------------------------
	public Member(String nm, String addr, int year, int month, int date) {

		name        = nm;
		address     = addr;
		dateOfBirth = Calendar.getInstance();
		dateOfBirth.set(year, month, date);

		numberBorrowed = 0;
		fineDue = 0.0;

	        // Calculate age (simplified calculation!)
        	int age = Calendar.getInstance().get(Calendar.YEAR) - year;

	        if (age >= 16)
            		memType = new AdultMembership();
	        else
        		memType = new ChildMembership(age);
	}


	// Method to recognise when borrower borrows a book
	// ---------------------------------------------------------------
        public void borrowedItem() {

	        numberBorrowed++;
	}


	// Method to recognise when borrower returns a book
	// ---------------------------------------------------------------
        public void returnedItem(double fine) {

	        numberBorrowed--;
		fineDue += fine;
	}


	// Determine whether the borrower can borrow any more books
	// ---------------------------------------------------------------
        public boolean canBorrow() {

	        if (fineDue < 2.50)  {

        		return true;
		}
		else {

			return false;
		}
	}
	
	
	// Accessor method to find out what fine is due
	// ---------------------------------------------------------------
	public double getFineDue() {

		return fineDue;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private String   name;
	private String   address;
	private Calendar dateOfBirth;

	private int      numberBorrowed;
	private double   fineDue;

	private MembershipType memType;
}
