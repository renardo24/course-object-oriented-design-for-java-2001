//========================================================================
//
// Filename:     Test.java
//
// Description:  Simple test harness (Java application)
//
//========================================================================


public class Test  {

	public static void main(String[] args) {

		// Create three members (an adult, a young child, and an older child)
		Member adult = new Member("Mr. Smith", 
		                          "Cirencester", 
		                          1954, 1, 1);

	    	Member youngChild = new Member("Dan Williams", 
		                               "Tetbury", 
		                               1994, 1, 1);

		Member olderChild = new Member("Sophie Watson", 
		                               "Swindon", 
		                               1986, 1, 1);
    

		// Try to borrow 7 items for adult (6 allowed)
		System.out.println("Adult borrows some books... ");
		for (int i = 1; i <= 7; i++) {

		        if (adult.canBorrow()) {
				System.out.println("Adult borrowed item " + i);
				adult.borrowedItem();
			}
			else {
				System.out.println("Adult could not borrow item " + i);
			}
		}

    
		// Try to borrow 3 items for young child (2 allowed)
		System.out.println("Young child borrows some books... ");
		for (int i = 1; i <= 3; i++) {

		        if (youngChild.canBorrow()) {
				System.out.println("Young child borrowed item " + i);
				youngChild.borrowedItem();
			}
			else {
				System.out.println("Young child could not borrow item " + i);
			}
		}


		// Try to borrow 5 items for older child (4 allowed)
		System.out.println("Older child borrows some books... ");
		for (int i = 1; i <= 5; i++) {

		        if (olderChild.canBorrow()) {
				System.out.println("Older child borrowed item " + i);
				olderChild.borrowedItem();
			}
			else {
				System.out.println("Older child could not borrow item " + i);
			}
		}


		// Return some books with a fine payable 
		// Fines should be waived for child members
	    	System.out.println("\nReturning some books late...");

		adult.returnedItem(1.25);
    		System.out.println("Fine due for adult " + adult.getFineDue());

		youngChild.returnedItem(0.85);
	    	System.out.println("Fine due for young child " + youngChild.getFineDue());

		olderChild.returnedItem(1.45);
	    	System.out.println("Fine due for older child " + olderChild.getFineDue());
 
	}
}


