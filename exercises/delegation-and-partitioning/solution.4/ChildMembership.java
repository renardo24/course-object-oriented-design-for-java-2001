//========================================================================
//
// Filename:     ChildMembership.java
//
// Description:  Simple version of the ChildMembership concrete class
//
//========================================================================


public class ChildMembership implements MembershipType {

	// Constructor
	// ---------------------------------------------------------------
	public ChildMembership(int a) {

		age = a;
	}


	// Implement the methods from the MembershipType interface
	// ---------------------------------------------------------------
        public int maximumBorrowable() {
	
	        if (age >= seniorAge)
        		return booksPerSenior;
	        else
        		return booksPerJunior;
	}

	
	public boolean areFinesApplicable() {

		return false;
	}
        

	// Private instance variables
	// ---------------------------------------------------------------
	private int age;


	// Private class contants
	// ---------------------------------------------------------------
	private static final int booksPerJunior = 2;
	private static final int booksPerSenior = 4;
	private static final int seniorAge      = 11;
}


