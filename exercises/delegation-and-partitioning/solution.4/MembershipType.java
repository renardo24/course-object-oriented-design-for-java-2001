//========================================================================
//
// Filename:     MembershipType.java
//
// Description:  Simple version of the MembershipType interface
//
//========================================================================


interface MembershipType {

        int     maximumBorrowable();
        boolean areFinesApplicable();
}