//========================================================================
//
// Filename:     Loan.java
//
// Description:  Each Loan represents one Book borrowed by one Member.
//
//========================================================================

import java.util.*;
import javax.swing.*;	// For Swing message box

public class Loan implements Reminder {

	// Constructor
	// ---------------------------------------------------------------
	public Loan(Member m, Book b)
	{
		theMember  = m;
		theBook    = b;
		expiryDate = QADate.getFutureSystemDate(14);
		ID         = Integer.toString(nextID++);
		ReminderService rs = ReminderService.getInstance();
		rs.attach( this, expiryDate );
	}


	// Instance method to play the role of a "destructor"
	// ---------------------------------------------------------------
	public void cancel()
	{
		ReminderService rs = ReminderService.getInstance();
		rs.detach( this );
	}


	// Accessor method to get the Loan ID
	// ---------------------------------------------------------------
	public String getID() {

		return ID;
	}


	// Interface method to be notified of any update
	// ---------------------------------------------------------------
	public void update()
	{
		JOptionPane.showMessageDialog( null,
		                               "Member: " + theMember + "\n" + "Book: " + theBook + "\n" + "Due: " + expiryDate,
		                               "Loan overdue!",
		                               JOptionPane.PLAIN_MESSAGE );
	}

	// Accessor method to return Loan details as a String
	// ---------------------------------------------------------------
	public String toString() {

		return "Member: " + theMember + "\n" +
                       "Book:   " + theBook   + "\n" +
		       "Due:    " + expiryDate;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private Member theMember;
	private Book   theBook;
	private QADate expiryDate;
	private String ID;


	// Private class variables
	// ---------------------------------------------------------------
	private static int nextID = 0;
}


