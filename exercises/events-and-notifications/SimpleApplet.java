//========================================================================
//
// Filename:     SimpleApplet.java
//
// Description:  Simple applet, to test the Library System
//
//========================================================================

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class SimpleApplet extends Applet implements ActionListener {

	// init() method initialises the UI, and creates sample objects
	// ---------------------------------------------------------------
	public void init() {

		initUserInterface();
		initLibrary();
	}


	// Private method, initialises the UI appearance
	// ---------------------------------------------------------------
	private void initUserInterface() {

		// First, add the List that shows available books
		availableList = new List(10);
		availableList.addActionListener(this);
		add(availableList);


		// Next, add the List that shows borrowed books
		borrowedList = new List(10);
		borrowedList.addActionListener(this);
		add(borrowedList);


		// Next, add a Button to artificially increment the system date
		tickerButton = new Button("Next day");
		tickerButton.addActionListener(this);
		add(tickerButton);


		// Finally, add a Label displaying the current system date
		dateLabel = new Label(QADate.getCurrentSystemDate().toString());
		add(dateLabel);
	}


	// Private method, creates library-related objects
	// ---------------------------------------------------------------
	private void initLibrary() {

		// First, create the Library object itself
		theLibrary = new Library("Cirencester Central");


		// Next, create a Member object and add to the Library
		member = new Member("John Smith");
		theLibrary.join(member);


		// Finally, create some Book objects, add to the Library,
		// and display in the "available books" List
		books    = new Book[6];
		books[0] = new Book("Rainbow Six");
		books[1] = new Book("The Firm");
		books[2] = new Book("Friends");
		books[3] = new Book("Wanderlust");
		books[4] = new Book("Hills");
		books[5] = new Book("The Client");

		for (int i = 0; i < books.length; i++) {
			theLibrary.addBook(books[i]);
			availableList.add(books[i].toString());
		}
	}


	// actionPerformed() method, called to handle ActionEvents
	// ---------------------------------------------------------------
	public void actionPerformed(ActionEvent e) {

		// Was it a double-click in the "available books" List?
		if (e.getSource() == availableList) {

			int    selIndex = availableList.getSelectedIndex();
			String selItem  = availableList.getSelectedItem();

			if (theLibrary.borrowItem(member.getID(),
			                          selItem.substring(0,1)) == true) {

				availableList.remove(selItem);
				borrowedList.add(selItem);
			}
		}
		// Was it a double-click in the "borrowed books" List?
		else if (e.getSource() == borrowedList) {

			int    selIndex = borrowedList.getSelectedIndex();
			String selItem  = borrowedList.getSelectedItem();

			if (theLibrary.returnItem(member.getID(),
			                          selItem.substring(0,1)) == true) {

				borrowedList.remove(selItem);
				availableList.add(selItem);
			}
		}
		// Was it a click on the "Next day" pushbutton?
		else if (e.getSource() == tickerButton) {

			QADate.incrementCurrentSystemDate();
			dateLabel.setText(QADate.getCurrentSystemDate().toString());

			// *** Un-comment this code in Question 2 ***
			ReminderService rs = ReminderService.getInstance();
			rs.dateChanged(QADate.getCurrentSystemDate());
		}
	}



	// Private instance variables - User Interface objects
	// ---------------------------------------------------------------
	private List   availableList;
	private List   borrowedList;
	private Button tickerButton;
	private Label  dateLabel;


	// Private instance variables - Library-related objects
	// ---------------------------------------------------------------
	private Library theLibrary;
	private Member  member;
	private Book[]  books;
}


