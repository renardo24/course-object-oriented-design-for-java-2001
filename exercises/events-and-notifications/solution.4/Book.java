//========================================================================
//
// Filename:     Book.java
//
// Description:  Simple version of the Book class
//
//========================================================================

public class Book {

	// Constructor
	// ---------------------------------------------------------------
	public Book(String n) {

		name = n;
		ID = Integer.toString(nextID++);
	}


	// Accessor method to get the ID
	// ---------------------------------------------------------------
	public String getID() {

		return ID;
	}


	// Accessor method to return details as a String
	// ---------------------------------------------------------------
	public String toString() {

		return ID + " " + name;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private String name;
	private String ID;


	// Private class variables
	// ---------------------------------------------------------------
	private static int nextID = 0;
}


