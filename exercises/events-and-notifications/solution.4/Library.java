//========================================================================
//
// Filename:     Library.java
//
// Description:  Simple version of the Library class
//
//========================================================================

import java.util.*;

public class Library {
	
	// Constructor
	// ---------------------------------------------------------------
	public Library(String n) {

		members = new Hashtable();
		books   = new Hashtable();
		loans   = new Hashtable();
	}
		

	// Member joins the Library
	// ---------------------------------------------------------------
	public boolean join(Member theMember) {

		boolean result = false;

		// Make sure Member object isn't in Library already
		if (members.contains(theMember) == false) {

			// Store Member in the members Hashtable
			members.put(theMember.getID(), theMember);
			result = true;
		}
		return result;
	}


	// Book is added to the Library
	// ---------------------------------------------------------------
	public boolean addBook(Book theBook) {

		boolean result = false;

		// Make sure Book object isn't in Library already
		if (books.contains(theBook) == false) {

			// Store Book in the books Hashtable
			books.put(theBook.getID(), theBook);
			result = true;
		}
		return result;
	}


	// Member borrows a Book from the Library
	// ---------------------------------------------------------------
	public boolean borrowItem(String memberID, String bookID) {

		boolean result = false;

		// Make sure Member and Book objects are in the library, and
		// also that the book isn't already on loan
		Member theMember = (Member)members.get(memberID);
		Book   theBook   = (Book)books.get(bookID);

		if (theMember != null && 
                    theBook   != null &&
                    loans.containsKey(bookID) == false) {

			// Create Loan object and store it in the loans Hashtable
			// For simplicity, identify the Loan by the Book's ID

			Loan newLoan = new Loan(theMember, theBook);
			loans.put(bookID, newLoan);

			result = true;
		}
		return result;
	}


	// Member returns a Book to the Library
	// ---------------------------------------------------------------
	public boolean returnItem(String memberID, String bookID) {

		boolean result = false;

		// Make sure Member and Book objects are in the library, and
		// also that the book is actually on loan
		Member theMember = (Member)members.get(memberID);
		Book   theBook   = (Book)books.get(bookID);
		Loan   theLoan   = (Loan)loans.get(bookID);

		if (theMember != null &&
                    theBook   != null &&
                    theLoan   != null) {

			// Cancel the loan
			theLoan.cancel();

			// Remove the Loan from the loans Hashtable
			loans.remove(bookID);
			result = true;
		}

		return result;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private Hashtable members;
	private Hashtable books;
	private Hashtable loans;
}


