//========================================================================
//
// Filename:     Loan.java
//
// Description:  Each Loan represents one Book borrowed by one Member.
//               The Loan class implements the Reminder interface, so that
//               it can be informed when reminders are issued.
//
//========================================================================

import java.util.*;
import javax.swing.*;	// For Swing message box

public class Loan implements Reminder {

	// Constructor
	// ---------------------------------------------------------------
	public Loan(Member m, Book b) {

		theMember  = m;
		theBook    = b;
		expiryDate = QADate.getFutureSystemDate(14);
		ID         = Integer.toString(nextID++);

		ReminderService rs = ReminderService.getInstance();
		rs.attach(this, expiryDate);
	}


	// Instance method to play the role of a "destructor"
	// ---------------------------------------------------------------
	public void cancel() {

		ReminderService rs = ReminderService.getInstance();
		rs.detach(this);
	}


	// Callback method, invoked by ReminderService if loan is overdue
	// ---------------------------------------------------------------
	public void update() {

		QADate today = QADate.getCurrentSystemDate();

		QADate reminder1 = expiryDate;
		QADate reminder2 = expiryDate.addDays(10);
		QADate reminder3 = expiryDate.addDays(17);

		ReminderService rs = ReminderService.getInstance();

		if (today.compare(reminder1) == true) {

			JOptionPane.showMessageDialog(null, 
		        	                      this.toString(),
			                              "First reminder!",
			                              JOptionPane.PLAIN_MESSAGE);

			rs.attach(this, reminder2);
		}

		else if (today.compare(reminder2) == true) {

			JOptionPane.showMessageDialog(null, 
		        	                      this.toString(),
			                              "Second reminder!!",
			                              JOptionPane.PLAIN_MESSAGE);

			rs.attach(this, reminder3);
		}
		else if (today.compare(reminder3) == true) {

			JOptionPane.showMessageDialog(null, 
		        	                      this.toString(),
			                              "Third reminder!!!",
			                              JOptionPane.PLAIN_MESSAGE);

			// We could bar the member at this point ... We'll leave that
			// as an exercise for the reader (i.e. you!)
		}
	}


	// Accessor method to get the Loan ID
	// ---------------------------------------------------------------
	public String getID() {

		return ID;
	}


	// Accessor method to return Loan details as a String
	// ---------------------------------------------------------------
	public String toString() {

		return "Member: " + theMember + "\n" + 
                       "Book:   " + theBook   + "\n" +
		       "Due:    " + expiryDate;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private Member theMember;
	private Book   theBook;
	private QADate expiryDate;
	private String ID;


	// Private class variables
	// ---------------------------------------------------------------
	private static int nextID = 0;
}


