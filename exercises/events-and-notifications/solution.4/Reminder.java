//========================================================================
//
// Filename:     Reminder.java
//
// Description:  Defines the Reminder interface, which plays the role 
//               of the "observer" in the Observer pattern.
//
//               Also see the ReminderService class, which plays the role
//               of the "supplier" in the Observer pattern.
//
//               Also see the Loan class, which plays the role of the 
//               "consumer" in the Observer pattern.
//
//========================================================================

public interface Reminder {
	
	void update();
}