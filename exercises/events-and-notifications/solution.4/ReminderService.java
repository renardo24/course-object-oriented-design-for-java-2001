//========================================================================
//
// Filename:     ReminderService.java
//
// Description:  Holds different types of Reminders ("observers"), which are 
//               updated at specified dates. 
//               Whenever the system date changes, the ReminderService checks 
//               each Reminder to see if it's due to be notified on this date.  
//               If it is, the ReminderService "updates" the Reminder, 
//               indicating that the required date has arrived.
//
//========================================================================

import java.util.*;

public class ReminderService {
	
        // Add another Reminder to the reminders Hashtable
	// ---------------------------------------------------------------
        public void attach(Reminder what, QADate when) {
	
		reminders.put(what, when);
	}


        // Remove a Reminder from the reminders Hashtable 
	// ---------------------------------------------------------------
        public void detach(Reminder what) {
	
                reminders.remove(what);
	}


        // Check all Reminders every time the system date changes, in case
        // any Reminders are due to be "updated" on this date!
	// ---------------------------------------------------------------
	public void dateChanged(QADate today) {

                // Loop through all the Reminder objects in the Hashtable
		Enumeration e = reminders.keys();
		while (e.hasMoreElements() == true) {

                        Reminder reminder = (Reminder)e.nextElement();
                        QADate   when     = (QADate)reminders.get(reminder);

			// If the Reminder is due on this day...
			if (today.compare(when) == true) {

                                // Issue a one-shot reminder
                                detach(reminder);
				reminder.update();
			}
		}
	}

	
        // ReminderService is a Singleton class, so the constructor
        // is private to prevent direct instantiation
	// ---------------------------------------------------------------
        private ReminderService() {

                 reminders = new Hashtable();
        }

        // Class method, returns a reference to the one-and-only
        // ReminderService object
	// ---------------------------------------------------------------
        public static ReminderService getInstance() {

                if (theReminderService == null) {

                        theReminderService = new ReminderService();
                }
                return theReminderService;
        }


	// Private instance variables
	// ---------------------------------------------------------------
        Hashtable reminders;


        // Private class variables
	// ---------------------------------------------------------------
        static ReminderService theReminderService;
}
