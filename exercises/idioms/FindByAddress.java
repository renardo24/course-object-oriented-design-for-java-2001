//========================================================================
//
// Filename:     FindByAddress.java
//
// Description:  This class implements the "Command" interface. 
//               The "execute" method compares a Member's address to see
//               if it matches the one specified beforehand.
//
//========================================================================


public class FindByAddress implements Command {
	
	// Store the "address" of the member we're going to search for
	// (we could have done this in a constructor)
	// ---------------------------------------------------------------
	public void setTarget(String address) {
		
		targetAddress = address;
	}


	// Implement the "execute" method, to compare a Member's address 
	// to see if it matches the one specified beforehand
	// ---------------------------------------------------------------
	public boolean execute(Member aMember) {

		String memberAddress = aMember.getAddress();

		if (memberAddress.equals(targetAddress))
			return true;
		else
			return false;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	String targetAddress;
}
