//========================================================================
//
// Filename:     FindByName.java
//
// Description:  This class implements the "Command" interface. 
//               The "execute" method compares a Member's name to see
//               if it matches the one specified beforehand.
//
//========================================================================


public class FindByName implements Command {
	
	// Store the "name" of the member we're going to search for
	// (we could have done this in a constructor)
	// ---------------------------------------------------------------
	public void setTarget(String name) {
		
		targetName = name;
	}


	// Implement the "execute" method, to compare a Member's name 
	// to see if it matches the one specified beforehand
	// ---------------------------------------------------------------
	public boolean execute(Member aMember) {

		String memberName = aMember.getName();

		if (memberName.equals(targetName))
			return true;
		else
			return false;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	String targetName;
}
