import java.util.*;
import java.text.DateFormat;

public class QADate {


	// Constructor from three integers
	// ---------------------------------------------------------------
	public QADate(int year, int month, int date) {

		theCalendar = Calendar.getInstance();
		theCalendar.set(year, month, date);
	}


	// Constructor from a string (must be in yyyy/mm/dd format)
	// ---------------------------------------------------------------
	public QADate(String text) {

		String yyyy = text.substring(0, 4);
		String mm   = text.substring(5, 7);
		String dd   = text.substring(8, 10);

		theCalendar = Calendar.getInstance();

            // month is zero based

		theCalendar.set(Integer.parseInt(yyyy),
		                Integer.parseInt(mm)-1,
		                Integer.parseInt(dd));
	}


	// Instance method, to return date in human-readable form
	// ---------------------------------------------------------------
	public String toString() {

		DateFormat dt = DateFormat.getDateInstance(DateFormat.LONG);
		dt.setCalendar(theCalendar);
		return dt.format(theCalendar.getTime());
	}


	// Instance method, returns a new QADate object "numDays" greater
	// than "this" QADate object ("this" QADate is not changed)
	// ---------------------------------------------------------------
	public QADate addDays(int numDays) {

		QADate newDate = new QADate( theCalendar.get(Calendar.YEAR),
		                             theCalendar.get(Calendar.MONTH),
		                             theCalendar.get(Calendar.DAY_OF_MONTH));

		newDate.theCalendar.add(Calendar.DAY_OF_MONTH, numDays);

		return newDate;
	}


	// Instance method, to compare this date with another
	// ---------------------------------------------------------------
	public boolean compare(QADate other) {

		return (theCalendar.get(Calendar.YEAR) ==
                              other.theCalendar.get(Calendar.YEAR)
		          &&

		        theCalendar.get(Calendar.DAY_OF_YEAR) ==
                              other.theCalendar.get(Calendar.DAY_OF_YEAR));
	}


	// Static method to get current system date
	// ---------------------------------------------------------------
	static public QADate getCurrentSystemDate() {

		return new QADate(systemDate.get(Calendar.YEAR),
		                  systemDate.get(Calendar.MONTH),
		                  systemDate.get(Calendar.DAY_OF_MONTH));
	}


	// Static method to get future system date (in "numDays" days time)
	// ---------------------------------------------------------------
	static public QADate getFutureSystemDate(int numDays) {

		return getCurrentSystemDate().addDays(numDays);
	}


	// Static method to increment system date by one day
	// ---------------------------------------------------------------
	static public void incrementCurrentSystemDate() {

		systemDate.add(Calendar.DAY_OF_MONTH, 1);
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private Calendar theCalendar;


	// Private class variables and initialisation
	// ---------------------------------------------------------------
	private static Calendar systemDate = Calendar.getInstance();

}