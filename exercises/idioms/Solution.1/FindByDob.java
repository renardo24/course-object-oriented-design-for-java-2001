//========================================================================
//
// Filename:     FindByDob.java
//
// Description:  This class implements the "Command" interface. 
//               The "execute" method compares a Member's date of birth to see
//               if it matches the one specified beforehand.
//
//========================================================================


public class FindByDob implements Command {
	
	// Store the "dob" of the member we're going to search for
	// (we could have done this in a constructor)
	// ---------------------------------------------------------------
	public void setTarget(String date) {
		
		targetDob = new QADate(date);
	}


	// Implement the "execute" method, to compare a Member's dob 
	// to see if it matches the one specified beforehand
	// ---------------------------------------------------------------
	public boolean execute(Member aMember) {

		QADate memberDob = aMember.getDob();

		if (memberDob.compare(targetDob) == true)
			return true;
		else
			return false;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	QADate targetDob;
}
