//========================================================================
//
// Filename:     FindByID.java
//
// Description:  This class implements the "Command" interface. 
//               The "execute" method compares a Member's ID to see
//               if it matches the one specified beforehand.
//
//========================================================================


public class FindByID implements Command {
	
	// Store the "ID" of the member we're going to search for
	// (we could have done this in a constructor)
	// ---------------------------------------------------------------
	public void setTarget(String ID) {
		
		targetID = ID;
	}


	// Implement the "execute" method, to compare a Member's ID 
	// to see if it matches the one specified beforehand
	// ---------------------------------------------------------------
	public boolean execute(Member aMember) {

		String memberID = aMember.getID();

		if (memberID.equals(targetID))
			return true;
		else
			return false;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	String targetID;
}
