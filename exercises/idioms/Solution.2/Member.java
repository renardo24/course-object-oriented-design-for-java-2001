//========================================================================
//
// Filename:     Member.java
//
// Description:  Simple version of the Member class
//
//========================================================================

public class Member {

	// Constructor
	// ---------------------------------------------------------------
	public Member(String n, String a, int year, int month, int date) {

		name = n;
		address = a;
		dob = new QADate(year, month, date);

		ID = Integer.toString(nextID++);
	}


	// Accessor method to get the name
	// ---------------------------------------------------------------
	public String getName() {

		return name;
	}


	// Accessor method to get the address
	// ---------------------------------------------------------------
	public String getAddress() {

		return address;
	}


	// Accessor method to get the date of birth
	// ---------------------------------------------------------------
	public QADate getDob() {

		return dob;
	}


	// Accessor method to get the ID
	// ---------------------------------------------------------------
	public String getID() {

		return ID;
	}


	// Accessor method to return details as a String
	// ---------------------------------------------------------------
	public String toString() {

		return name           + "\n" +
		       address        + "\n" +
		       dob.toString() + "\n" +
		       "ID:  " + ID     + "\n";
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private String name;
	private String address;
	private QADate dob;
	private String ID;


	// Private class variables
	// ---------------------------------------------------------------
	private static int nextID = 0;
}


