//========================================================================
//
// Filename:     SimpleApplet.java
//
// Description:  Simple applet, to test the Library System
//
//========================================================================

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;	  // For Swing message box


public class SimpleApplet extends Applet implements ActionListener {
	
	// init() method initialises the UI, and creates sample objects  
	// ---------------------------------------------------------------
	public void init() {

		initUserInterface();
		initLibrary();
	}


	// Private method, initialises the UI appearance
	// ---------------------------------------------------------------
	private void initUserInterface() {

		// Add the text field where the user can type a search string
		searchTextField = new TextField(20);
		add(searchTextField);


		// Next, add 4 buttons to execute a search against the members'
		// name, address, date-of-birth, or ID respectively
		Panel panel = new Panel();

		nameButton = new Button(" Name  ");
		nameButton.addActionListener(this);
		panel.add(nameButton);

		addressButton = new Button("Address");
		addressButton.addActionListener(this);
		panel.add(addressButton);

		dobButton = new Button(" D.o.b.");
		dobButton.addActionListener(this);
		panel.add(dobButton);

		idButton = new Button(" ID No. ");
		idButton.addActionListener(this);
		panel.add(idButton);

		add(panel);


		// Set the focus to the text field initially
		searchTextField.requestFocus();
	}


	// Private method, creates library-related objects
	// ---------------------------------------------------------------
	private void initLibrary() {

		// First, create the Library object itself
		theLibrary = new Library("Cirencester Central");


		// Next, create some Member objects and add to the Library
		theLibrary.join( new Member("John Smith",
		                            "4 The Circus, Bath",
		                            1964, 11, 3 ) );

		theLibrary.join( new Member("Sue Jones",
		                            "9 High Street, York",
		                            1965, 0, 19) );

		theLibrary.join( new Member("Bill Evans",
		                            "5 Heath Close, Leeds",
		                            1974, 4, 12) );

		theLibrary.join( new Member("Hazel Thomas",
		                            "10 Oxford Road, Poole",
		                            1979, 11, 11) );

		theLibrary.join( new Member("Phil James",
		                            "6 Church Lane, Exeter",
		                            1962, 0, 31) );

		theLibrary.join( new Member("Mary Rees",
		                            "2 Chipping Row, Yate",
		                            1972, 6, 29) );

	}		


	// actionPerformed() method, called to handle ActionEvents
	// ---------------------------------------------------------------
	public void actionPerformed(ActionEvent e) {

		// Extract the text from the TextField
		String text = searchTextField.getText();


		// Declare a Command reference, then create appropriate type of
		// Command object based on which button was pressed
		Command cmd = null;


		if (e.getSource() == nameButton) {

			cmd = new Command() {

				public void setTarget(String name) {		
					targetName = name;
				}

				public boolean execute(Member aMember) {
					return aMember.getName().equals(targetName);
				}
				private String targetName;
			};
		}
		else if (e.getSource() == addressButton) {

			cmd = new Command() {

				public void setTarget(String address) {		
					targetAddress = address;
				}

				public boolean execute(Member aMember) {
					return aMember.getAddress().equals(targetAddress);
				}
				private String targetAddress;
			};
		}
		else if (e.getSource() == dobButton) {

			cmd = new Command() {

				public void setTarget(String dob) {		
					targetDob = new QADate(dob);
				}

				public boolean execute(Member aMember) {
					return aMember.getDob().equals(targetDob);
				}
				private QADate targetDob;
			};
		}
		else if (e.getSource() == idButton) {

			cmd = new Command() {

				public void setTarget(String ID) {		
					targetID = ID;
				}

				public boolean execute(Member aMember) {
					return aMember.getID().equals(targetID);
				}
				private String targetID;
			};
		}


		// Set the target for the command, then do the search!
		cmd.setTarget(text);
		Member member = theLibrary.findMember(cmd);
		

		// Check whether the member was found or not
		if (member == null) {

			JOptionPane.showMessageDialog(null, 
		        	                      "No member found with details: " + text,
			                              "Member not found!",
			                              JOptionPane.PLAIN_MESSAGE);
		}
		else {

			JOptionPane.showMessageDialog(null, 
		        	                      member.toString(),
			                              "Member found!",
			                              JOptionPane.PLAIN_MESSAGE);
		}


		// Tidy up the user interface
		searchTextField.requestFocus();
		searchTextField.setText("");
	}

	

	// Private instance variables - User Interface objects
	// ---------------------------------------------------------------
	private TextField searchTextField;
	private Button nameButton, addressButton, dobButton, idButton;


	// Private instance variables - Library-related objects
	// ---------------------------------------------------------------
	private Library theLibrary;
}


