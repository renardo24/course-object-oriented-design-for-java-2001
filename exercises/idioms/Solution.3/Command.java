//========================================================================
//
// Filename:     Command.java
//
// Description:  The "Command" interface
//
//========================================================================


interface Command {
	
	// All implementing classes must implement the following methods
	// ---------------------------------------------------------------
	void setTarget(String target);
	boolean execute(Member nextMember);
}
