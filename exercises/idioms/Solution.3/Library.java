//========================================================================
//
// Filename:     Library.java
//
// Description:  Simple version of the Library class
//
//========================================================================

import java.util.*;

public class Library {
	
	// Constructor
	// ---------------------------------------------------------------
	public Library(String n) {

		members = new HashSet();
	}
		

	// Member joins the Library
	// ---------------------------------------------------------------
	public boolean join(Member theMember) {

		boolean result = false;

		// Make sure Member object isn't in Library already
		if (members.contains(theMember) == false) {

			// Store Member in the members Set
			members.add(theMember);
			result = true;
		}
		return result;
	}


	// Find a Member by executing a "find" command
	// ---------------------------------------------------------------
	public Member findMember(Command command) {

		Iterator i = members.iterator();

		while (i.hasNext() == true) {
		
			// Check the next member - is this the one we're looking for?
			Member member = (Member)i.next();

			if (command.execute(member) == true) {
	
				// This is the member we want, so return this Member!
				return member;
			}
		}
	
		// If we get this far, the member wasn't found so return null instead
		return null;
	}


	// Private instance variables
	// ---------------------------------------------------------------
	private HashSet members;
}


