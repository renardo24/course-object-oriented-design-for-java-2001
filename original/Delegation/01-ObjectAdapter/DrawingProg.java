/**
 *  DrawingProg.java
 *  Test harness to demonstrate Object Adapter Design Pattern,
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class DrawingProg {
	public static void main (String[] args) {
		Rectangle rectangle = new Rectangle();
		Ellipse ellipse = new Ellipse();
		Text text = new Text();

		rectangle.draw();
		ellipse.draw();
		text.draw();
	}
}