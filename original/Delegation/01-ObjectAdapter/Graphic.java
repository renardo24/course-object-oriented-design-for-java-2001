/**
 *  Graphic.java
 *  Demonstrate the Composite design pattern.  This interface is
 *  the base component.  It can be extended into a leaf (Ellipse,
 *  Rectangle, etc) or a group (which in turn can be a Graphic).
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Graphic {
	public void draw();
}