/**
 *  Rectangle.java
 *  Demonstrate the Object Adapter Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Rectangle implements Graphic {
	public void draw() {
		System.out.println("Rectangle.draw() called");
	}
}