import java.util.*;
/**
 *  Text.java
 *  Demonstrate the Object Adapter design pattern.  This is the adapter.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Text implements Graphic {
	private SuperText superText;

	public Text() {
		superText = new SuperText();
	}

	public void draw() {
		System.out.println("Text.draw() called");
		superText.display();
	}
}