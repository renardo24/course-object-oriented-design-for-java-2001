/**
 *  Text.java
 *  Demonstrate the Class Adapter design pattern.  This is the adapter.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Text extends SuperText implements Graphic {
	// This constructor isn't really necessary - it is automatic.
	public Text() {
		super();
	}

	public void draw() {
		System.out.println("Text.draw() called");
		super.display();
	}
}