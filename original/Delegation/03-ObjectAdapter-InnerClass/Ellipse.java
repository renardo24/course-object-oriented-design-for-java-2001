/**
 *  Ellipse.java
 *  Demonstrate the Object Adapter Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Ellipse implements Graphic {
	public void draw() {
		System.out.println("Ellipse.draw() called");
	}
}