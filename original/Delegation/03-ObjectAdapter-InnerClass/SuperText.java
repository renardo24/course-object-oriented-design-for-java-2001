/**
 *  SuperText.java
 *  Demonstrate the Object Adapter Design Pattern.  The SuperText
 *	class is an existing class that we would like to reuse in
 *	another class.  It is the adaptee.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class SuperText {
	public void display () {
		System.out.println("SuperText.display() called");
	}
}