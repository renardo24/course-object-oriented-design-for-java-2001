/**
 *  Text.java
 *  Demonstrate the Object Adapter design pattern, using an inner class.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Text implements Graphic {
	private InnerSuperText superText = new InnerSuperText();
	public void draw() {
		System.out.println("Text.draw() called");
		superText.display();
	}
	/**************** Inner class ****************/
	private class InnerSuperText extends SuperText {
		public void display() {
			super.display();
		}
	}
}