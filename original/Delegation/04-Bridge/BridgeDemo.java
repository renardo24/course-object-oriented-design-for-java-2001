/**
 *  BridgeDemo.java
 *  Demonstrate the Bridge design pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class BridgeDemo {
	public static void main (String[] args) {
		Database payroll = new Payroll(new JDBCAccess());
		Database purchase = new Purchase(new DirectAccess());

		payroll.openConnection();
		purchase.openConnection();
	}
}