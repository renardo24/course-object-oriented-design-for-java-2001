/**
 *  DBAccess.java
 *  Demonstrate the Bridge design pattern.  This is the interface to the
 *	creation classes - JDBCAccess and DirectAccess classes.  These are
 *	effectively the Adaptees to the Adapter (the Database class).
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface DBAccess {
	public abstract void findConnection();
}