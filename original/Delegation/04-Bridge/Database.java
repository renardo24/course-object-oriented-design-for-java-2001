/**
 *  Database.java
 *  Demonstrate the Bridge design pattern.  Database is the Adapter to
 *	DBAccess, the Adaptee.  Database is also the bridge between its
 *	child classes (Payroll and Purchase) and DBAccess.  Database
 *	exposes the openConnection() operation to its users.  But how it
 *	is implemented is hidden in the body of the method.  (We can see
 *	below that we "adapt" the findConnection() method in the
 *	openConnection() method.)
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Database {
	private DBAccess dbAccess = null;

	public Database (DBAccess dbAccess) {
		this.dbAccess = dbAccess;
	}
	public void openConnection() {
		dbAccess.findConnection();
	}
}