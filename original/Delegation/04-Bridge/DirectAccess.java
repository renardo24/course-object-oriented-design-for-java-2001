/**
 *  DirectAccess.java
 *  Demonstrate the Bridge design pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class DirectAccess implements DBAccess {
	public void findConnection() {
		System.out.println("DirectAccess.findConnection() called");
	}
}