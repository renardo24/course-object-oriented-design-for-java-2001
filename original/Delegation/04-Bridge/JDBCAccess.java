/**
 *  JDBCAccess.java
 *  Demonstrate the Bridge design pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class JDBCAccess implements DBAccess {
	public void findConnection() {
		System.out.println("JDBCAccess.findConnection() called");
	}
}