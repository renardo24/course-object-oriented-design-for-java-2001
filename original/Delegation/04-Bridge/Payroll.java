/**
 *  Payroll.java
 *  Demonstrate the Bridge design pattern. Payroll simply uses the
 *	DBAccess interface.  The implementation of the openConnection()
 *	operation is delegated to its super class. I.e. the implementation
 *	of this method is hidden from Payroll.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Payroll extends Database {
	public Payroll (DBAccess dbAccess) {
		super(dbAccess);
	}
	public void openConnection() {
		super.openConnection();
	}
}