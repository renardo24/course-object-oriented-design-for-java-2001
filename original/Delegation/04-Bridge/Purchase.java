/**
 *  Purchase.java
 *  Demonstrate the Bridge design pattern.  Purchase simply uses the
 *	DBAccess interface.  The implementation of the openConnection()
 *	operation is delegated to its super class. I.e. the implementation
 *	of this method is hidden from Purchase.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Purchase extends Database {
	public Purchase (DBAccess dbAccess) {
		super(dbAccess);
	}
	public void openConnection() {
		super.openConnection();
	}
}