When an abstraction can have one of several possible implementations,
the usual way to accomodate them is to use inheritance.  An abstract
class defines the inheritance to the abstraction, and concrete sub-
classes implement it in different ways.  But this approach isn't
always flexible enough.  Inheritance binds an implementation to the
abstraction permanently, which makes it difficult to modify, extend
and reuse abstractions and implementations independently.

The Bridge Pattern
==================
The Bridge Pattern dcouples an abstraction from its implementation
so that the 2 can vary independently.

This is also known as the Handle/Body idiom.

Use the Bridge pattern when:

*  you want to avoid a permanent binding between an abstraction and
   its implementation.  This might be the case, for example, when
   the implementation must be selected or switched at run-time.
   
*  both the abstractions and their implementations should be exten-
   sible by subclassing.  In this case, the Bridge pattern lets you
   combine the different abstractions and implementations and extend
   them independently.
   
*  changes in the implementation of an abstraction should have no
   impact on clients. i.e. their code should not have to be recompiled.
   
*  you want to hide the implementation of an abstraction completely
   from clients.
   
*  you have a proliferation of classes. Deeply inherited trees indicate
   the need for splitting an object into 2 parts.
   
*  you want to share an implementation among multiple objects.


Excerpts from "Design Patterns" by Gamma et al.

Nic Nei
(nnei@compuserve.com)