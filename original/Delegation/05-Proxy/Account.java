/**
 *  Account.java
 *  Demonstrate the Proxy Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Account {
	public void withdraw (double amount);
}