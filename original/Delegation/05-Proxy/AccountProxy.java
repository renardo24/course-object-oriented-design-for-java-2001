/**
 *  AccountProxy.java
 *  Demonstrate the Proxy Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class AccountProxy implements Account {
	private Account target = null;

	public void withdraw (double amount) {
		if (target == null)
			target = new PersistentAccount();
		target.withdraw(amount);
	}
}