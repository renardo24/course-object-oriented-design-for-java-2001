/**
 *  Customer.java
 *  Demonstrate the Proxy Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Customer {
	public static void main (String[] args) {
		Account account = new AccountProxy();

		account.withdraw(123.45);
	}
}