/**
 *  PersistentAccount.java
 *  Demonstrate the Proxy Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class PersistentAccount implements Account {
	public void withdraw (double amount) {
		System.out.println("Withdraw "+amount+" from PersistentAccount");
	}
}