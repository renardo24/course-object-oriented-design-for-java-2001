/**
 *  Account.java
 *  Demonstrate the Vertical Delegation Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public abstract class Account {
	protected double balance;

	protected abstract boolean canWithdraw(double amount);
	public abstract void withdraw (double amount);
}