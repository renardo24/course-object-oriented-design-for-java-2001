/**
 *  ChequeAccount.java
 *  Demonstrate the Vertical Delegation Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ChequeAccount extends Account {
	private double overdraftLimit = -100;

	public ChequeAccount (double balance) {
		this.balance = balance;
	}
	protected boolean canWithdraw (double amount) {
		if (balance - amount <= overdraftLimit) {
			System.out.println("Withdrawing "+amount+" not allowed for cheque acount");
			return false;
		}
		return true;
	}
	public void withdraw (double amount) {
		System.out.println("Withdraw "+amount+" from ChequeAccount");
		if (canWithdraw(amount))
			balance -= amount;
	}
}