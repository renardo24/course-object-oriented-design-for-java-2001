/**
 *  Customer.java
 *  Demonstrate the Vertical Delegation Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Customer {
	public static void main (String[] args) {
		Account chequeAccount = new ChequeAccount(100);
		chequeAccount.withdraw(123.45);

		System.out.println();
		Account savingsAccount = new SavingsAccount(10);
		savingsAccount.withdraw(123.45);
	}
}