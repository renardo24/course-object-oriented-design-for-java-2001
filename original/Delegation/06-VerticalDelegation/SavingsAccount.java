/**
 *  SavingsAccount.java
 *  Demonstrate the Vertical Delegation Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class SavingsAccount extends Account {
	public SavingsAccount (double balance) {
		this.balance = balance;
	}
	protected boolean canWithdraw (double amount) {
		if (balance - amount <= 0) {
			System.out.println("Withdrawing "+amount+" not allowed for savings acount");
			return false;
		}
		return true;
	}
	public void withdraw (double amount) {
		System.out.println("Withdraw "+amount+" from SavingsAccount");
		if (canWithdraw(amount))
			balance -= amount;
	}
}