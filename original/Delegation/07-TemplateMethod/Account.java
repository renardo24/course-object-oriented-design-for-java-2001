/**
 *  Account.java
 *  Demonstrate the Template Method Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public abstract class Account {
	protected Money balance;

	protected abstract boolean canWithdraw(Money amount);
	public abstract void withdraw (Money amount);
}