/**
 *  AccountProxy.java
 *  Demonstrate the Proxy Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public abstract class AccountProxy extends Account {
	private Account target = null;
	private double balance;

	public AccountProxy (double balance) {
		this.balance = balance;
	}

	public void withdraw (double amount) {
		if (target == null)
			target = new PersistentAccount(balance);
		target.withdraw(amount);
	}
}