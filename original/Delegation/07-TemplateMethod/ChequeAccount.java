/**
 *  ChequeAccount.java
 *  Demonstrate the Template Method Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ChequeAccount extends Account {
	private Money overdraftLimit = new Money(-100.0);

	public ChequeAccount (Money balance) {
		this.balance = new Money(balance);
	}
	protected boolean canWithdraw (Money amount) {
		Money possibleBalance = balance.subtract(amount);

		if (possibleBalance.lessThanOrEqual(overdraftLimit)) {
			System.out.println("Withdrawing "+amount+" not allowed for cheque acount");
			return false;
		}
		return true;
	}
	public void withdraw (Money amount) {
		System.out.println("Withdraw "+amount+" from ChequeAccount");
		if (canWithdraw(amount))
			balance = balance.subtract(amount);
	}
}