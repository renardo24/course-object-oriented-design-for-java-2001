/**
 *  Customer.java
 *  Demonstrate the Template Method Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Customer {
	public static void main (String[] args) {
		Money balance = new Money(100.0);
		Money debit = new Money(123.45);
		System.out.println("Value of balance: "+balance);
		System.out.println("Value of withdrawal: "+debit);

		Account chequeAccount = new ChequeAccount(balance);
		chequeAccount.withdraw(debit);

		System.out.println();
		Account savingsAccount = new SavingsAccount(balance);
		savingsAccount.withdraw(debit);
	}
}