/**
 *  Money.java
 *  Demonstrate the Template Method Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Money {
	private double value;

	public Money (double value) {
		this.value = value;
	}

	public Money (Money money) {
		this.value = money.value;
	}

	public Money add (Money amount) {
		return new Money (amount.value + value);
	}

	public Money subtract (Money amount) {
		return add(new Money(-amount.value));
	}

	public boolean lessThanOrEqual (Money amount) {
		return this.value <= amount.value;
	}

	public String toString() {
		return ""+value;
	}
}