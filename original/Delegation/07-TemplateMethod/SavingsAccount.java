/**
 *  SavingsAccount.java
 *  Demonstrate the Template Method Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class SavingsAccount extends Account {
	public SavingsAccount (Money balance) {
		this.balance = new Money(balance);
	}
	protected boolean canWithdraw (Money amount) {
		if (amount.lessThanOrEqual(balance))
			return true;
		System.out.println("Withdrawing "+amount+" not allowed for savings acount");
		return false;
	}
	public void withdraw (Money amount) {
		System.out.println("Withdraw "+amount+" from SavingsAccount");
		if (canWithdraw(amount))
			balance = balance.subtract(amount);
	}
}