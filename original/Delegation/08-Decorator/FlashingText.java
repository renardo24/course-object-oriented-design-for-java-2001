/**
 *  FlashingText.java
 *  Demonstrate the Decorator Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class FlashingText {
	private String body;

	public FlashingText (Text text) {
		this.body = text.toString();
	}

	public String toString() {
		return "Flash! Flash! " + body;
	}
}