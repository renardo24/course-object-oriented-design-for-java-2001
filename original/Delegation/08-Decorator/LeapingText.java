/**
 *  LeapingText.java
 *  Demonstrate the Decorator Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class LeapingText {
	private String body;

	public LeapingText (String string) {
		this.body = string;
	}

	public LeapingText (Text text) {
		this.body = text.toString();
	}

	public LeapingText (FlashingText flashingText) {
		this(flashingText.toString());
	}

	public LeapingText (ScreamingText screamingText) {
			this(screamingText.toString());
	}

	public String toString() {
		return "Leap! Leap! " + body;
	}
}