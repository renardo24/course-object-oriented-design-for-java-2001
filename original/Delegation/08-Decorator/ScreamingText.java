/**
 *  ScreamingText.java
 *  Demonstrate the Decorator Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ScreamingText {
	private String body;

	public ScreamingText (String string) {
		this.body = string;
	}

	public ScreamingText (Text text) {
		this.body = text.toString();
	}

	public ScreamingText (FlashingText flashingText) {
		this(flashingText.toString());
	}

	public String toString() {
		return "Scream! Scream! " + body;
	}
}