/**
 *  Text.java
 *  Demonstrate the Decorator Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Text {
	private String body;

	public Text (String source) {
		body = new String(source);
	}

	public String toString() {
		return new String(body);
	}
}