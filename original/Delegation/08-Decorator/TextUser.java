/**
 *  TextUser.java
 *  Demonstrate the Decorator Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class TextUser {
	public static void main (String[] args) {
		Text text = new Text("I am Text");
		System.out.println(text+" <--- Text");

		FlashingText flashingText = new FlashingText(text);
		System.out.println(flashingText+" <--- FlashingText");

		ScreamingText screamingText = new ScreamingText(flashingText);
		System.out.println(screamingText+" <--- ScreamingText");

		LeapingText leapingText = new LeapingText(screamingText);
		System.out.println(leapingText+" <--- LeapingText");
	}
}