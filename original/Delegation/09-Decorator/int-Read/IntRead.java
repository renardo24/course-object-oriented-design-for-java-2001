import java.io.*;
/**
 * Title:        IntRead<p>
 * Description:  Demonstrate reading int primitives from a file<p>
 * Copyright:    Copyright (c) Nic Nei<p>
 * Company:      Java Nuts Inc<p>
 * @author Nic Nei
 * @version 1.0
 */
public class IntRead {
    public static void main (String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("file-of-ints");
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);

        int number = dataInputStream.readInt();
        System.out.println(""+number);
        number = dataInputStream.readInt();
        System.out.println(""+number);
        number = dataInputStream.readInt();
        System.out.println(""+number);

        fileInputStream.close();
    }
}