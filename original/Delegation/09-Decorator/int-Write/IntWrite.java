import java.io.*;
/**
 * Title:        IntWrite<p>
 * Description:  Demonstrate writing int primitives to a file<p>
 * Copyright:    Copyright (c) Nic Nei<p>
 * Company:      Java Nuts Inc<p>
 * @author Nic Nei
 * @version 1.0
 */
public class IntWrite {
    public static void main (String[] args) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("file-of-ints");
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);

        dataOutputStream.writeInt(111);
        dataOutputStream.writeInt(222);
        dataOutputStream.writeInt(333);
        dataOutputStream.writeInt(444);

        dataOutputStream.flush();
        fileOutputStream.close();
    }
}