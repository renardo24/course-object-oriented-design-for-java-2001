/**
 *  DrawingProg.java
 *  Test harness to demonstrate Composite Design Pattern,
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class DrawingProg {
	public static void main (String[] args) {
		Rectangle rectangle = new Rectangle();
		Ellipse ellipse = new Ellipse();

		rectangle.move(1,2);
		ellipse.move(3,4);

		System.out.println();
		Group group = new Group();
		group.add(new Rectangle());
		group.add(new Rectangle());
		group.add(new Ellipse());
		group.move(5,6);
	}
}