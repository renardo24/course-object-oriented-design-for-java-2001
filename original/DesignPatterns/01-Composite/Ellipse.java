/**
 *  Ellipse.java
 *  Demonstrate the Composite design pattern.  This is the leaf.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Ellipse implements Graphic {
	public void move (int x, int y) {
		System.out.println("Ellipse.move("+x+","+y+") called");
	}
}