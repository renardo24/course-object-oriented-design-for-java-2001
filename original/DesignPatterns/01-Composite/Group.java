import java.util.*;
/**
 *  Group.java
 *  Demonstrate the Composite design pattern.  This is the Group
 *  which may compose other Graphic objects (which may in turn be
 *  Group or leaf objects).
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Group implements Graphic {
	Vector collection = new Vector();

	public void move (int x, int y) {
		System.out.println("Group.move("+x+","+y+") called");
		System.out.println("Moving the Composite objects...");
		for (Iterator iterator = collection.iterator(); iterator.hasNext(); ) {
			Graphic graphic = (Graphic)iterator.next();
			graphic.move(x,y);
		}
	}
	public void add (Graphic graphic) {
		collection.add(graphic);
	}
}