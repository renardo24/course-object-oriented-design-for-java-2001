/**
 *  Rectangle.java
 *  Demonstrate the Composite design pattern.  This is the leaf.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Rectangle implements Graphic {
	public void move (int x, int y) {
		System.out.println("Rectangle.move("+x+","+y+") called");
	}
}