/**
 *  Command.java
 *  Demonstrate the Command design pattern.  This is the interface.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Command {
	public void execute();
	public void undo();
}