/**
 *  DrawingProg.java
 *  Test harness to demonstrate Command Design Pattern,
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class DrawingProg {
	public static void main (String[] args) {
		Move move1 = new Move(new Rectangle(),10,20);
		Move move2 = new Move(new Ellipse(),30,40);

		System.out.println("About to move 2 Graphic objects...");
		move1.execute();
		move2.execute();

		System.out.println("\nAbout to undo the move...");
		move1.undo();
		move2.undo();
	}
}