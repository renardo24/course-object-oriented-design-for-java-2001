/**
 *  Move.java
 *  Demonstrate the Command design pattern. The Move class
 *  encapsulates the Command class by implementing it.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Move implements Command {
	private Graphic target;
	private int dx;
	private int dy;

	public Move (Graphic target, int dx, int dy) {
		this.target = target;
		this.dx = dx;
		this.dy = dy;
	}

	public void execute() {
		target.move(dx,dy);
	}

	public void undo() {
		target.move(-dx,-dy);
	}
}