/**
 *  Cut.java
 *  Demonstrate the Command Processor Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Cut implements Edit {
	private int start, end;
	private Text target;
	private String scrap;

	public Cut (Text target, int start, int end) {
		this.target = target;
		this.start = start;
		this.end = end;
	}
	public void execute() {
		scrap = target.cut(start,end);
	}

	public void undo() {
		target.undo(scrap);
	}
}