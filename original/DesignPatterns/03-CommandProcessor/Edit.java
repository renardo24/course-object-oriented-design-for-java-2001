/**
 *  Edit.java
 *  Demonstrate the Command Processor Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Edit {
	public void execute();
	public void undo();
}