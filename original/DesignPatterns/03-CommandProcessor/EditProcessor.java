import java.util.*;
/**
 *  EditProcessor.java
 *  Demonstrate the Command Processor Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class EditProcessor {
	private Stack history;
	private int editCount;

	public EditProcessor() {
		editCount = 0;
		history = new Stack();
	}

	public void execute (Edit edit) {
		edit.execute();
		history.push(edit);
		++editCount;
		System.out.println("EditProcessor.execute() called " + editCount +
			" time" + (editCount==1?"":"s") + "\n");
	}

	public void undo() {
		Edit edit;

		if (history.empty()) {
			System.out.println("No more undo() operations left");
			return;
		}

		edit = (Edit)history.pop();
		edit.undo();
		System.out.println("EditProcessor.undo() called");
	}
}