/**
 *  EditProg.java
 *  Test harness to test Command Processor Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class EditProg {
	public static void main (String[] args) {
		Text text1 = new Text("Hello");
		Text text2 = new Text("Goodbye");

		EditProcessor editProcessor = new EditProcessor();
		editProcessor.execute (new Cut(text1,2,4));
		editProcessor.execute (new Cut(text2,5,6));

		System.out.println("After Cut()...");
		System.out.println(text1);
		System.out.println(text2);

		System.out.println();
		editProcessor.undo();
		editProcessor.undo();
		editProcessor.undo();

		System.out.println("After undo()...");
		System.out.println(text1);
		System.out.println(text2);
	}
}