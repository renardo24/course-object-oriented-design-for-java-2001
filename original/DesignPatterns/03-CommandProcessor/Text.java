/**
 *  Text.java
 *  Demonstrate the Command Processor Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Text {
	private int start, end;
	private StringBuffer body;

	public Text (String source) {
		body = new StringBuffer(source);
	}
	public String cut (int start, int end) {
		String scrap = body.substring(start,end);
		body.delete(start,end);
		this.start = start;
		this.end = end;
		return new String(scrap);
	}
	public void undo (String scrap) {
		body.insert(start,scrap);
	}
	public String toString() {
		return body.toString();
	}
}