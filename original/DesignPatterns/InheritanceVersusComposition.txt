Inheritance Versus Composition
==============================
The two most common techniques for reusing functionality in
object-oriented systems are class inheritance and object
composition.  Class inheritance lets you define the
implementation of one class in terms of another's.  Reuse
by subclassing is often referred to as *white-box reuse*.
The term "white-box" refers to visibility; With inheritance,
the internals of parent classes are often visible to sub-
classes.

Object composition is an alternative to class inheritance.
Here, new functionality is obtained by assembling or composing
objects to get more complex functionality.  Object composition
requires that the objects being composed have well-defined
interfaces.  This style of reuse is often known as *black-box
reuse*, because no internal details are visible.  Objects
appear only as "black-boxes".

Inheritance and composition each have their advantages and
disadvantages.  Class inheritance is defined statically at
compile-time and is straightforward to use, since it's
supported directly by the programming language.  Class
inheritance also makes it easier to modify the implementation
being reused.  When a subclass overrides some but not all the 
operations, it can affect the operations it inherits as well,
assuming they call the overridden operations.

But class inheritance has some disadvantages, too.  First,
you can't change the implementations inherited from parent
classes at run-time, because inheritance is defined at
compile-time.  Second, and generally worse, parent classes
often define at least part of their sub-classes' physical
representation.  Because inheritance exposes a subclass to
details of its parent's implementation, it's often said 
that "inheritance breaks encapsulation".  The implementation
of a subclass becomes so bound up with the implementation
of its parent class that any change in the parent's imple-
mentation will force the subclass to change.

Implementation dependencies can cause problems when you're
trying to reuse a subclass.  Should any aspect of the inher-
ted implementation not be appropriate for new problem
domains, the parent class must be rewritten or replaced by
something more appropriate.  This dependency limits
flexibility and ultimately reusability. One cure for this
is to inherit only from abstract classes, since they usually
provide little or no implementation.

Object composition is defined dynamically at run-time through
objects acquiring references to other objects. Composition
requires objects to respect each other's interfaces, which in
turn requires carefully designed interfaces that don't stop
you from using one object with many others.  But there is a
payoff.  Because objects are accessed solely through their
interfaces, we don't break encapsulation.  Any object can be
replaced at run-time by another as long as it has the same
type.  Moreover, because an object's implementation will be
written in terms of object interfaces, there are sunstantially
fewer implementation dependencies.

Object composition has another effect on system design.  Fav-
ouring object composition over class inheritance helps you
keep each class encapsulated and focused on one task.  Your
classes and class hierarchies will remain small and will be
less likely to grow into unmanageable monsters.  On the other
hand, a design based on object composition will have more
objects (if fewer classes), and the system's behaviour will
depend on their interrelationships instead of being defined in
one class.

This leads us to our second principle of object-oriented design:

     Favour object composition over class inheritance.
     
Ideally, you shouldn't have to create new components to achieve
reuse.  You should be able to get all the functionality you
need just be assembling exisiting components through object
composition.  But this is rarely the case, because the set of
available components is never quite rich enough in practice.
Reuse by inheritance makes it easier to make new components
that can be composed with old ones.  Inheritance and object
composition thus work together.

Nevertheless, our experience is that designers overuse inheritance
as a reuse technique and designs are often made more reusable
(and simpler) by depending more on object composition.  You'll see
object composition applied again and again in design patterns.

Extracted from "Design Patterns" by Gamma et al.

Nic Nei.
(nnei@compuserve.com)