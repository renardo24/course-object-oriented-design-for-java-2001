public class ExceptionDemo {
    public static void main (String[] args) {
        try {
            myMethod();
        }
        catch (Exception ex) {
            System.err.println ("Exception occurred");
        }
        catch (MyException ex) {
            System.err.println ("Error is: " + ex);
        }
        try {
            System.in.read();
        }
        catch (Exception ex) {
            System.err.println ("IO Exception occurred");
            System.exit(1);
        }
    }
    public static void myMethod() throws MyException {
        System.out.println ("Inside myMethod()");
        throw new MyException ("I am about to throw!");
    }
}

class MyException extends Exception {
    private String errorname;
    public MyException (String name) {
        errorname = name;
    }
    public String toString() {
        return errorname;
    }
}