/**
 *  FunnyMoney.java
 *  Demonstrate the use overriding of equals() and hashCode(). FunnyMoney
 *	does not properly implement (by overriding) the equals() method.  So
 *	comparing 2 objects of this class with identical content would return
 *	false.  Not what you would expect, unless this is deliberate.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class FunnyMoney {
	private double value;
	public FunnyMoney (double value) {
		this.value = value;
	}
	public double getValue() {
		return value;
	}
}