/**
 *  ProperMoney.java
 *  Demonstrate the use overriding of equals() and hashCode().
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ProperMoney {
	private double value;
	public ProperMoney (double value) {
		this.value = value;
	}
	public double getValue() {
		return value;
	}
	private long toBits() {
		return Double.doubleToLongBits(value);
	}
	/**
	 * The value returned by hashCode is an int that maps an object
	 * into a bucket in a hash table. An object must always produce
	 * the same hash code. However, objects can share hash codes
	 * (they aren't necessarily unique). Writing a "correct" hashing
	 * function is easy--always return the same hash code for the same
	 * object. Hashing functions should be "efficient" - for instance
	 * the hash code for an Integer is its integer value.
	 *
	 * See (http://java.sun.com/docs/books/tutorial/java/javaOO/objectclass.html)
	 * for fuller discussion.
	 *
	 * In ProperMoney, value is a real number. To hash this value,
	 * we return the mantissa of a double.  This is bits 52 to 0
	 * according to the IEEE 754 format for "double format" bit
     * layout. 53 bits would overflow an integer - the return type of
     * this hashCode() function.  The best we can do is the most
     * significant part of the mantissa - i.e. bits 31 to 0.
     *
     * The >> operator right-shifts the bits in an integer, propogating
     * the sign in the shift.
     *
     * The ^ operator is the exclusive OR operator.
     */
	public int hashCode() {
		return (int)(toBits() ^ (toBits()>>32));
	}
	public boolean equals (ProperMoney otherMoney) {
		return (otherMoney != null) && (otherMoney instanceof ProperMoney) &&
			(toBits() == otherMoney.toBits());
	}
}