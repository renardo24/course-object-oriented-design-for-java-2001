/**
 *  UseMoney.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class UseMoney {
	public static void main (String[] args) {
		FunnyMoney funnyMoney1 = new FunnyMoney(12);
		FunnyMoney funnyMoney2 = new FunnyMoney(12);
		FunnyMoney funnyMoney1Alias = funnyMoney1;

		System.out.println("funnyMoney1.hashCode() returns: "+funnyMoney1.hashCode());
		System.out.println("funnyMoney2.hashCode() returns: "+funnyMoney2.hashCode());
		System.out.println("funnyMoney1.getValue() returns: "+funnyMoney1.getValue());
		System.out.println("funnyMoney2.getValue() returns: "+funnyMoney2.getValue());
		System.out.println("funnyMoney1 == funnyMoney2 returns: "+ (funnyMoney1 == funnyMoney2));
		System.out.println("funnyMoney1.equals(funnyMoney2) returns: "+ (funnyMoney1.equals(funnyMoney2)));

		System.out.println("funnyMoney1Alias == funnyMoney1 returns: "+ (funnyMoney1Alias == funnyMoney1));
		System.out.println("funnyMoney1Alias.equals(funnyMoney1) returns: "+ (funnyMoney1Alias.equals(funnyMoney1)));

		System.out.println();
		ProperMoney properMoney1 = new ProperMoney(12);
		ProperMoney properMoney2 = new ProperMoney(12);
		ProperMoney properMoney1Alias = properMoney1;

		System.out.println("properMoney1.hashCode() returns: "+properMoney1.hashCode());
		System.out.println("properMoney2.hashCode() returns: "+properMoney2.hashCode());
		System.out.println("properMoney1.getValue() returns: "+properMoney1.getValue());
		System.out.println("properMoney2.getValue() returns: "+properMoney2.getValue());
		System.out.println("properMoney1 == properMoney2 returns: "+ (properMoney1 == properMoney2));
		System.out.println("properMoney1.equals(properMoney2) returns: "+ (properMoney1.equals(properMoney2)));

		System.out.println("properMoney1Alias == properMoney1 returns: "+ (properMoney1Alias == properMoney1));
		System.out.println("properMoney1Alias.equals(properMoney1) returns: "+ (properMoney1Alias.equals(properMoney1)));
	}
}