/**
 *  BoundedStack.java
 *  Demonstrate the use of static inner classes as mini packages.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class BoundedStack {
	private int size = 0;
	private Object[] elements;

	public BoundedStack() {
		size = 0;
		elements = new Object[5];
	}

	public void push (Object newTop) {
		if (isFull())
			throw new Full();
		elements[size++] = newTop;
	}

	public Object pop() {
		if (isEmpty())
			throw new Empty();
		return elements[--size];
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean isFull() {
		return size == elements.length;
	}

	/*****************  static inner class *****************/
	public static class Full extends RuntimeException {
		public Full() {
			super("Stack is full");
		}
	}
	public static class Empty extends RuntimeException {
		public Empty() {
			super ("Stack is empty");
		}
	}
}