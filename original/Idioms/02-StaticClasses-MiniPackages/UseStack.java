/**
 *  UseStack.java
 *  Test harness to demonstrate use of static inner classes in BoundedStack.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class UseStack {
	public static void main (String[] args) {
		BoundedStack boundedStack = new BoundedStack();

		boundedStack.push("Jack");
		boundedStack.push("and");
		boundedStack.push("Jill");
		boundedStack.push("went");
		boundedStack.push("up");

		System.out.println((String)boundedStack.pop());
		System.out.println((String)boundedStack.pop());
		System.out.println((String)boundedStack.pop());
		System.out.println((String)boundedStack.pop());
		System.out.println((String)boundedStack.pop());

		try {
			System.out.println("About to overflow stack..");
			boundedStack.push("Jack");
			boundedStack.push("and");
			boundedStack.push("Jill");
			boundedStack.push("went");
			boundedStack.push("up");
			boundedStack.push("the");
			boundedStack.push("Hill");
		}
		catch (RuntimeException ex) {
			System.err.println(""+ex);
		}
	}
}