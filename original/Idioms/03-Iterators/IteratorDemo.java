import java.util.*;
/**
 *  IteratorDemo.java
 *  Demonstrate the use of iterators in the java.util package.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class IteratorDemo {
	public static void main (String[] args) {
		Vector collection = new Vector();

		collection.add("Jack");
		collection.add("and");
		collection.add("Jill");
		collection.add("Went");
		collection.add("Up");
		collection.add("The");
		collection.add("Hill");

		for (Iterator iterator = collection.iterator(); iterator.hasNext(); )
			System.out.print((String)iterator.next()+" ");
		System.out.println();

		for (Enumeration enum = collection.elements(); enum.hasMoreElements(); )
			System.out.print((String)enum.nextElement()+" ");
		System.out.println();
	}
}