import java.util.*;
/**
 *  BoundedStack.java
 *  Demonstrate the implementation of iterator using inner class.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class BoundedStack {
	private int size = 0;
	private Object[] elements;

	public BoundedStack() {
		size = 0;
		elements = new Object[10];
	}

	public void push (Object newTop) {
		if (isFull())
			throw new Full();
		elements[size++] = newTop;
	}

	public Object pop() {
		if (isEmpty())
			throw new Empty();
		return elements[--size];
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean isFull() {
		return size == elements.length;
	}

	public Enumeration elements() {
		return new Iterator();
	}

	/*****************   inner class iterator *****************/
	private class Iterator implements Enumeration {
		private int index = 0;
		public boolean hasMoreElements() {
			return index < size;
		}
		public Object nextElement() {
			if (!hasMoreElements())
				throw new NoMoreElements();
			return elements[index++];
		}
	}
	/*****************  static inner class *****************/
	public static class Full extends RuntimeException {
		public Full() {
			super("Stack is full");
		}
	}
	public static class Empty extends RuntimeException {
		public Empty() {
			super ("Stack is empty");
		}
	}
	public static class NoMoreElements extends RuntimeException {
		public NoMoreElements() {
			super ("No more elements to iterate");
		}
	}
}