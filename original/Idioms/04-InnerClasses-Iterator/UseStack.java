import java.util.*;
/**
 *  UseStack.java
 *  Test harness to demonstrate use of inner classes in BoundedStack.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class UseStack {
	public static void main (String[] args) {
		BoundedStack boundedStack = new BoundedStack();

		boundedStack.push("Jack");
		boundedStack.push("and");
		boundedStack.push("Jill");
		boundedStack.push("went");
		boundedStack.push("up");
		boundedStack.push("the");
		boundedStack.push("Hill");

		for (Enumeration enum= boundedStack.elements(); enum.hasMoreElements(); )
			System.out.print((String)enum.nextElement()+" ");
		System.out.println();
	}
}