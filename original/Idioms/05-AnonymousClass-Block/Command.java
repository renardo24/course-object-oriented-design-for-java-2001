/**
 *  Command.java
 *  Command interface to demonstrate the Command Design Pattern.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Command {
	public void execute();
}