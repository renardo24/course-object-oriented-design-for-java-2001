import java.util.Date;
/**
 *  Schedule.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Schedule {
	public static void main (String[] args) {
		Schedule schedule = new Schedule();
		Date now = new Date();
		Date later = new Date(now.getTime()+10000L);
		Timer timer = new Timer();
		schedule.setAlarm(timer,later,"Hello world");
		System.out.println("timer will execute command in 10000 milliseconds...");
		timer.start();
	}
	/**
	 *
	 *	Use anonymous class to implement the Command interface.  This is
	 * 	a block of code that will be passed to the set() operation.
	 */
	public void setAlarm (Timer timer, Date when, final String message) {
		timer.set(when, new Command() {
			public void execute() {
				System.out.println(message);
			}
		});
	}
}
