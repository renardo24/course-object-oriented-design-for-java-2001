import java.util.Date;
/**
 *  Timer.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Timer implements Runnable {
	private Date when;
	private Command command;
	private Thread thread;

	public void set (Date when, Command command) {
		this.when = when;
		this.command = command;
	}
	public void start() {
		thread = new Thread(this);
    	thread.start();
	}
	public void run() {
		do {
			// Is it time to execute command yet?
			Date now = new Date();
			if (now.getTime() >= when.getTime()) {
				command.execute();
				break;
			}
			//  Sleep 1 second before checking if time has arrived.
			try {
				System.out.println("Sleep...");
				Thread.sleep(1000L);
			}
			catch (InterruptedException ex) {
				// do nothing
			}
		} while (true);
	}
}
