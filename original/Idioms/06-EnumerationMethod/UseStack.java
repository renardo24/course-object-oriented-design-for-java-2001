import java.util.*;
/**
 *  UseStack.java
 *  Test harness to demonstrate use of Enumeration Method idiom.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class UseStack {
	public static void main (String[] args) {
		BoundedStack boundedStack = new BoundedStack();

		boundedStack.push("Jack");
		boundedStack.push("and");
		boundedStack.push("Jill");
		boundedStack.push("went");
		boundedStack.push("up");
		boundedStack.push("the");
		boundedStack.push("Hill");

		boundedStack.forEachElementDo (new BoundedStack.Operation() {
			public void executeOn (Object object) {
				System.out.print(object+" ");
			}
		});

		// Because we have encapsulated the loop mechanism in the
		// BoundedStack class, we can specify the operation externally,
		// namely here.  In this example, the operation is reversePrint().
		System.out.println("\n\nNow in reverse...\n");
		boundedStack.forEachElementDo (new BoundedStack.Operation() {
			public void executeOn (Object object) {
				reversePrint((String)object);
			}
		});
		System.out.println();
	}

	public static void reversePrint (String string) {
		StringBuffer temp = new StringBuffer();
		for (int index = string.length() - 1; index >= 0; index--)
			temp.append(string.charAt(index));

  		System.out.print(temp.toString()+" ");
	}
}