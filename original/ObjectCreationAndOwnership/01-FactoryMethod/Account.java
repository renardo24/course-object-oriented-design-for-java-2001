/**
 *  Account.java
 *  Demonstrate the Factory Method.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Account {
	public void payin (double amount);
	public void withdraw (double amount);
	public String getName();
	public double getBalance();
}