/**
 *  Bank.java
 *  Demonstrate the Factory Method.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Bank {
	public Account openAccount (String name, double balance) {
		return new PersistentAccount(name,balance);
	}
}