/**
 *  PersistentAccount.java
 *  Demonstrate the Factory Method.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class PersistentAccount implements Account {
	private String name;
	private double balance;

	public PersistentAccount (String name, double openingBalance) {
		this.name = name;
		this.balance = openingBalance;
	}

	public void payin (double amount) {
		balance += amount;
	}

	public void withdraw (double amount) {
		balance -= amount;
	}

	public String getName() {
		return name;
	}

	public double getBalance() {
		return balance;
	}
}