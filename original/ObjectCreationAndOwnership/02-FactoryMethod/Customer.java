/**
 *  Customer.java
 *  Demonstrate the Factory Method.
 *
 *  Nic Nei (nnei@compuserve.com)
 */

import bank.Bank;
import bank.Account;
import bank.PersistentAccount;

public class Customer {
	public static void main (String[] args) {
		Bank bank = new Bank();

		Account account = bank.openAccount ("Joe Bloggs", 100.10);

		System.out.println("New account opened for "+account.getName());
		System.out.println("With initial balance of "+account.getBalance());

		// This should fail because PersistantAccount is package-visibility.
		// Account account = new PersistentAccount("Fred Flintstone", 123.45);
	}
}