
public class Aircraft extends AltitudeEventSource
{
    private int height;
    private String id;
    
    public Aircraft(String s, int h)
    {
        id = s;
        height = h;
    }
    public void ascend(int h)
    {
        height += h;
        notifyHeight(height);
    }
    public void descend(int h)
    {
        height -= h;
        notifyHeight(height);
    }
    public String getID()
    {
        return id;
    }


}