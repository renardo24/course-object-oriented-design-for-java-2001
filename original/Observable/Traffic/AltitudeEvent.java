class AltitudeEvent extends java.util.EventObject
{
    private int height;
    
    public AltitudeEvent(Object o, int h)
    {
        super(o);
        height = h;
    }
    public int getHeight()
    {
        return height;
    };
    
}