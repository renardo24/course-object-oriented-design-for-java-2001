public abstract class AltitudeEventSource extends java.util.Observable
{
    protected void notifyHeight(int h)
    {
        setChanged();
        notifyObservers(new AltitudeEvent(this, h));
    }
}