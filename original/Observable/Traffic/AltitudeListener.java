
public interface AltitudeListener extends java.util.EventListener
{
    public void altitudeChanged(AltitudeEvent e);  
}