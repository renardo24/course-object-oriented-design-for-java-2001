import java.util.*;

public abstract class AltitudeObserver implements Observer, AltitudeListener
{
   
    public void update(Observable ob, Object o)
    {
        AltitudeEvent ae = (AltitudeEvent)o;
        altitudeChanged(ae);
    }
    
    public abstract void altitudeChanged(AltitudeEvent e);  
}