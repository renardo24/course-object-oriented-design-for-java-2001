import java.util.*;
class ControlTower extends AltitudeObserver implements AltitudeListener
{
    private String location;

    public ControlTower(String s)
    {
        location = s;
    }
    
    public void altitudeChanged(AltitudeEvent e)
    {
        Aircraft a = (Aircraft)e.getSource();
        String s = a.getID();
        int h = e.getHeight();
       System.out.println(location + ": " + s + " moved to " + h + "ft");
    }
}