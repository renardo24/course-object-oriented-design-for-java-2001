class TrafficTest
{
    public static void main(String[] args)
    {   
        Aircraft a = new Aircraft("BA1321", 10000);
        
        ControlTower t1;  
        a.addObserver(t1 = new ControlTower("Heathrow"));
        
        ControlTower t2;   
        a.addObserver(t2 = new ControlTower("Gatwick"));
              
        System.out.println("Changing height...");
        a.ascend(2000);

  	    try
		{System.in.read();}
		catch(Exception e){}      
        
    }
}