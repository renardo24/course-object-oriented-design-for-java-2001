import java.rmi.*;
/**
 *  Calculator.java
 *  This is just an interface to the client and the server.
 *  It basically establishes a contract between the two to
 *  say what services the Calculator class provides - add(),
 *  sub(), mul() and div()!
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Calculator extends Remote {
    public long add(long a, long b)throws RemoteException;
    public long sub(long a, long b)throws RemoteException;
    public long mul(long a, long b)throws RemoteException;
    public long div(long a, long b)throws RemoteException;
}