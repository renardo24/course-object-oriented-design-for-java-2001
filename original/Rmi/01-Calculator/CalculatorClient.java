import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  CalculatorClient.java
 *  I am the RMI client and I want to use the services of a remote object of type
 *  Calculator which can be retrieved using the lookup() method of the static
 *  Naming class.
 *
 *  I expect to find the CalculatorService on a machine named localhost.  I will
 *  (by default) look for the service at port number 1099.  If this is not the
 *  port number then I will not be able to reach the server.  BTW, the server
 *  name is CalculatorService.
 *
 *  The server has contracted (by the Calculator.java interface) that it provides
 *  add(), sub(), mul(), etc services in the Calculator.  Let's see if they work...
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class CalculatorClient {
  public static void main(String[] args) {
    try {
	  System.setSecurityManager(new RMISecurityManager());
      Calculator c = (Calculator)Naming.lookup("rmi://localhost/CalculatorService");
      System.out.println(c.sub(4,3));
      System.out.println(c.add(4,5));
      System.out.println(c.mul(3,6));
      System.out.println(c.div(9,3));
    }
    catch (MalformedURLException murle) {
      System.out.println();
      System.out.println("MalformedURLException");
      System.out.println(murle);
    }
    catch (RemoteException re) {
      System.out.println();
      System.out.println("RemoteException");
      System.out.println(re);
    }
    catch (NotBoundException nbe) {
      System.out.println();
      System.out.println("NotBoundException");
      System.out.println(nbe);
    }
    catch (java.lang.ArithmeticException ae) {
      System.out.println();
      System.out.println("java.lang.ArithmeticException");
      System.out.println(ae);
    }
  }
}