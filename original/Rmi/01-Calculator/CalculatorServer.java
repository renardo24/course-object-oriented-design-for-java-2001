import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;

/**
 *  CalculatorServer.java
 *  I am the server and extends the UnicastRemoteObject, meaning I will be a
 *  non-replicated non-persistent remote object to the client (on another
 *  machine).  I will also implement the Calculator interface, meaning I will
 *  fulfill my contract to fulfill the add(), sub(), div(), etc services.
 *
 *  At the start of the program, I will register myself as CalculatorService
 *  on the server named localhost, listening for clients to use my service
 *  at port 1099 (which happens to be the default, by the way).
 *
 *  I also depend on the register myself on the bootstrap registry.  I can
 *  do this myself using LocateRegistry.createRegistry(1099) or somebody else
 *  (the user), will run rmregistry before I start.  I am assuming that you
 *  will do this please, because I can't be bothered.
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class CalculatorServer extends UnicastRemoteObject implements Calculator {
	public CalculatorServer() throws RemoteException {
		super();
	}
	public static void main(String args[]) {
		try {
			LocateRegistry.createRegistry(1099);
			System.setSecurityManager(new RMISecurityManager());
			Calculator server = new CalculatorServer();
			Naming.rebind("rmi://localhost:1099/CalculatorService", server);
			System.out.println("I am willing and ready to serve...");
		}
		catch (RemoteException ex) {
			System.err.println("Nobody knows the super() trouble I'm in.."+ex);
		}
		catch (MalformedURLException ex) {
			System.err.println("Nobody knows the rebind() trouble I'm in.."+ex);
		}
	}
	/**
	  *  I am the server and I have contracted to implement these methods
	  *  in the Calculator interface.  I expect my client(s) (Calculator.client)
	  *  to use the following services...
	  */
	public long add(long a, long b)throws java.rmi.RemoteException {
    		return a + b;
  	}
  	public long sub(long a, long b) throws java.rmi.RemoteException {
    		return a - b;
  	}
  	public long mul(long a, long b) throws java.rmi.RemoteException {
    		return a * b;
  	}
  	public long div(long a, long b)throws java.rmi.RemoteException {
    		return a / b;
  	}
  	// there can be other methods, but these cannot be used by the
  	// the client.
}