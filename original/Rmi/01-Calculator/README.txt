This is a very simple Remote Method Invocation demo.
The server is CalculatorServer and the client is
CalculatorClient.  Basically, CalculatorServer will
perform arithmetic calculations (remotely) for the
client.  Read the comments in each file for more
info.

There are 3 files in this demo:

Calculator.java
CalculatorServer.java
CalculatorClient.java

Order of compilation:

javac Calculator.java
javac CalculatorServer.java
rmic CalculatorServer
javac CalculatorClient.java

Better still, just use the Makefile by typing

nmake

It will automatically build EVERYTHING for you.

Order of running, open 2 windows, then:


(1) In one window:
	
	nmake runserver

(2) Open another window:
	
	nmake runclient


To terminate the rmiregistry and server, press CTRL-C.

Nic Nei (nnei@compuserve.com)
