mkdir C:\Server
copy CalculatorServer.class C:\Server
copy Calculator.class C:\Server
copy CalculatorServer_Skel.class C:\Server
copy CalculatorServer_Stub.class C:\Server
copy POLICY C:\Server
cd C:\Server
java -Djava.rmi.server.codebase=file:/Server/ -Djava.security.policy=policy CalculatorServer