import java.rmi.*;
/**
 *  Hello.java
 *  This is just an interface to the client and the server.
 *  It basically establishes a contract between the two to
 *  say what services the Hello class provides - sayHello()!
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Hello extends Remote {
    public String sayHello()throws RemoteException;
}