import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  HelloClient.java
 *  I am the RMI client and I want to use the services of a remote object of type
 *  Hello which can be retrieved using the lookup() method of the static
 *  Naming class.
 *
 *  I expect to find the HelloService on a machine named localhost.  I will
 *  (by default) look for the service at port number 1099.  If this is not the
 *  port number then I will not be able to reach the server.  BTW, the service
 *  name is HelloService.
 *
 *  The server has contracted (by the Hello.java interface) that it provides
 *  the sayHello() service.  Let's see if this works...
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class HelloClient {
  public static void main(String[] args) {
    try {
	  System.setSecurityManager(new RMISecurityManager());
      Hello hello = (Hello)Naming.lookup("rmi://localhost/HelloService");
      System.out.println(hello.sayHello());
    }
    catch (MalformedURLException murle) {
      System.out.println();
      System.out.println("MalformedURLException");
      System.out.println(murle);
    }
    catch (RemoteException re) {
      System.out.println();
      System.out.println("RemoteException");
      System.out.println(re);
    }
    catch (NotBoundException nbe) {
      System.out.println();
      System.out.println("NotBoundException");
      System.out.println(nbe);
    }
  }
}