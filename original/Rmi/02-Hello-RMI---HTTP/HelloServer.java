import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;

/**
 *  HelloServer.java
 *  I am the server and extends the UnicastRemoteObject, meaning I will be a
 *  non-replicated non-persistent remote object to the client (on another
 *  machine).  I will also implement the Hello interface, meaning I will
 *  fulfill my contract to fulfill the sayHello() service.
 *
 *  At the start of the program, I will register myself as HelloService
 *  on the server named localhost, listening for clients to use my service
 *  at port 1099 (which happens to be the default, by the way).
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class HelloServer extends UnicastRemoteObject implements Hello {
	public HelloServer() throws RemoteException {
		super();
	}
	public static void main(String args[]) {
		try {
			LocateRegistry.createRegistry(1099);
			System.setSecurityManager(new RMISecurityManager());
			Hello server = new HelloServer();
			Naming.rebind("rmi://LGJAVAV20/HelloService", server);
			System.out.println("I am willing and ready to serve...");
		}
		catch (RemoteException ex) {
			System.err.println("Nobody knows the super() trouble I'm in.."+ex);
		}
		catch (MalformedURLException ex) {
			System.err.println("Nobody knows the rebind() trouble I'm in.."+ex);
		}
	}
	/**
	  *  I am the server and I have contracted to implement these methods
	  *  in the Hello interface.  I expect my client(s) (Hello.client)
	  *  to use the following services...
	  */
	public String sayHello()throws java.rmi.RemoteException {
    		return "Greetings from an RMI Server's Chocolate Stash!";
  	}

  	// there can be other methods, but these cannot be used by the
  	// the client.
}