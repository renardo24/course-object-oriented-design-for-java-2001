This example is based on the earlier Hello World
RMI demo (01 Hello RMI).

The java code is essentially identical.  The
difference is in the naming service.  The server
previously (re)bind() the name to the local machine:

Naming.rebind("rmi://localhost/HelloService", server);

Now, it binds the name to another machine:

Naming.rebind("rmi://LGJAVAV20/HelloService", server);

Likewise for the client.  Previously:

Hello hello = (Hello)Naming.lookup("rmi://localhost/HelloService");

Now:

Hello hello = (Hello)Naming.lookup("rmi://LGJAVAV20/HelloService");

The server and clients will now run on *different* machines.
The client will need to have the stub *dynamically* downloaded
and the server machine must *offer* the stub to be downloaded.

This is how the server starts:

java -Djava.rmi.server.codebase=http://LGJAVAV20/MyChocolateStash/ 
	-Djava.security.policy=policy HelloServer
	
It is saying that the codebase (where the stub is) is relative
to a folder named MyChocolateStash on the LGJAVAV20 machine.

This is how the client starts:

java -Djava.security.policy=policy 
	-Djava.rmi.server.codebase=http://LGJAVAV20/MyChocolateStash/ 
	HelloClient
	
It says that the codebase (where it expects to find the stub)
is on LGJAVAV20's MyChocolateStash folder.

The server must offer the stubs to be downloaded using http,
and the client will download the stub over http.  This means
the Web server must offer the folder where the stubs are
as MyChocolateStash.  On IIS (if you are using it), you have
to make this a virtual folder.  See the GIF file where you find
this document for screenshots of what the setup on IIS manager 
looks likt.

To compile and run this demo, just use the Makefile by typing

nmake

It will automatically build EVERYTHING for you.

Order of running, open 2 windows, then:


(1) In one window on machine LGJAVAV20 (the server):
	
	nmake runserver

(2) Open another window on a *different* machine (the client):
	
	nmake runclient


To terminate the rmiregistry and server, press CTRL-C.

Nic Nei (nnei@compuserve.com)
