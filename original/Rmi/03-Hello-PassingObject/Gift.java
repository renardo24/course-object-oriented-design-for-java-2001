import java.rmi.*;
import java.io.*;
/**
 *  Gift.java
 *  This is an object passed from client to server as a parameter
 *	via the sayHello() method.  The purpose of this demonstration
 *	is to show that an object passed as a parameter, or returned
 *	from a method will be passed by value when the object implements
 *	the Serializable interface.  Read the README file for more
 *	information.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
//public class Gift implements Remote, Serializable {
public class Gift implements Remote {
    private String message = "This is a gift object passed from client to server";
    public String getMessage() {
		return message;
	}
}