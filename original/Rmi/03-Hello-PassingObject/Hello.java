import java.rmi.*;
/**
 *  Hello.java
 *  This is the interface to the Hello service.  A client can
 *	contact this server and invoke the sayHello() operation
 *	with a Gift object.  The Gift object will be passed to
 *	the server by value - i.e. it must implement the
 *	Serializable interface.  Read the README file for more
 *	information.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface Hello extends Remote {
    public String sayHello(Gift gift)throws RemoteException;
}