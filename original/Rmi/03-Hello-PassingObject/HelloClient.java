import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  HelloClient.java
 *  This is the client to the HelloServer object.  The client
 *	rendezvous with the server using RMI.  The purpose of this
 *	demonstration is to show that an object passed as a parameter,
 *	or returned from a method will be passed by value when the
 *	object implements the Serializable interface.  Read the
 *	README file for more information.
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class HelloClient {
  public static void main(String[] args) {
	  Gift gift = new Gift();
    try {
	  System.setSecurityManager(new RMISecurityManager());
      Hello hello = (Hello)Naming.lookup("rmi://localhost/HelloService");
      System.out.println(hello.sayHello(gift));
    }
    catch (MalformedURLException murle) {
      System.out.println();
      System.out.println("MalformedURLException");
      System.out.println(murle);
    }
    catch (RemoteException re) {
      System.out.println();
      System.out.println("RemoteException");
      System.out.println(re);
    }
    catch (NotBoundException nbe) {
      System.out.println();
      System.out.println("NotBoundException");
      System.out.println(nbe);
    }
  }
}