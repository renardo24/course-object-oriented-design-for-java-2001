import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;

/**
 *  HelloServer.java
 *  This is the server and it implements the Hello interface.
 *	The purpose of this demonstration is to show that an object
 *	passed as a parameter, or returned from a method will be
 *	passed by value when the object implements the Serializable
 *	interface.  Read the README file for more information.
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class HelloServer extends UnicastRemoteObject implements Hello {
	public HelloServer() throws RemoteException {
		super();
	}
	public static void main(String args[]) {
		try {
			LocateRegistry.createRegistry(1099);
			System.setSecurityManager(new RMISecurityManager());
			Hello server = new HelloServer();
			Naming.rebind("rmi://localhost:1099/HelloService", server);
			System.out.println("I am willing and ready to serve...");
		}
		catch (RemoteException ex) {
			System.err.println("Nobody knows the super() trouble I'm in.."+ex);
		}
		catch (MalformedURLException ex) {
			System.err.println("Nobody knows the rebind() trouble I'm in.."+ex);
		}
	}
	/**
	  *  The server implements the Hello interface and so must
	  *	 implement the sayHello() method.
	  */
	public String sayHello(Gift gift)throws java.rmi.RemoteException {
			System.out.println("We have a gift: "+gift.getMessage());
    		return "Greetings from an RMI Server!";
  	}

  	// there can be other methods, but these cannot be used by the
  	// the client.
}