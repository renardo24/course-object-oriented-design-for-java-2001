This demonstration shows that an object passed as a
parameter to the method, or returned from a method
will be passed by value when it implements the 
Serializable interface.

This object is the Gift object.

The Gift.java does not implement the Serializable
interface, at the moment.  Everything in this
demonstration will compile, without errors. But
when you execute the client, this is what happens:

C>java -Djava.security.policy=policy HelloClient

RemoteException
java.rmi.MarshalException: error marshalling arguments; nested exception is: 
	java.io.NotSerializableException: Gift
	
To correct this, go to the Gift.java file and
remove the commented line that makes Gift implement
the Serializable interface:

public class Gift implements Remote, Serializable 

To compile and run this demonstration, just use the 
Makefile by typing

nmake

It will automatically build EVERYTHING for you.

Order of running, open 2 windows, then:


(1) In one window:
	
	nmake runserver

(2) Open another window:
	
	nmake runclient


To terminate the rmiregistry and server, press CTRL-C.

Nic Nei (nnei@compuserve.com)
