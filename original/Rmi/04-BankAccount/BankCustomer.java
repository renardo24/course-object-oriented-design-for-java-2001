import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  BankCustomer.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class BankCustomer {
  public static void main(String[] args) {
    try {
	  System.setSecurityManager(new RMISecurityManager());
      BankAccount bankAccount = (BankAccount)Naming.lookup("rmi://localhost/BankAccountService");
      System.out.println("Balance is "+bankAccount.getBalance());
      bankAccount.withdraw(55.2F);
      System.out.println("After withdrawing 55.2: "+bankAccount.getBalance());
      bankAccount.deposit(11.0F);
      System.out.println("After depositing 11.0: "+bankAccount.getBalance());
    }
    catch (Exception ex) {
      System.out.println("Exception: "+ex);
    }
  }
}