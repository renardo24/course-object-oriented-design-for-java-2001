04 BankAccount
==============
Demonstrate a simple RMI BankAccount.

BankAccount.java is the interface that extends Remote.  It
defines the services provided:

	public void deposit (float amount) throws RemoteException;
	public void withdraw (float amount) throws RemoteException;
	public float getBalance() throws RemoteException;
	
BankAccountImpl.java implements these servics, plus the
constructor:

	public BankAccountImpl (float balance) ...
	
BankAccountService.java creates the BankAccount and registers
it in the RMI registry, calling it "BankAccountService".

BankCustomer.java finds the BankAccount and uses its operations.

To try out this demo, do the following.

In one Command prompt - nmake
Then nmake runserver
In another Command prompt - nmake runclient

Nic Nei (nnei@compuserve.com)
