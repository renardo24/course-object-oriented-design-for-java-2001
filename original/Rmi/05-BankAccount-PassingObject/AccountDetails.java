import java.io.*;
/**
 *  AccountDetails.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
//public class AccountDetails implements {
public class AccountDetails implements Serializable {
	private String name;
	private int id;
	public AccountDetails (String name, int id) {
		this.name = name;
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public int getId() {
		return id;
	}
}