import java.rmi.*;
/**
 *  BankAccount.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface BankAccount extends Remote {
    public void deposit (float amount) throws RemoteException;
    public void withdraw (float amount) throws RemoteException;
    public float getBalance() throws RemoteException;
    public void setAccountDetails(AccountDetails accountDetails) throws RemoteException;
    public AccountDetails getAccountDetails() throws RemoteException;
}