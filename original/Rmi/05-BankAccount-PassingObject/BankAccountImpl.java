import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;
/**
 *  BankAccountImpl.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class BankAccountImpl extends UnicastRemoteObject implements BankAccount {
	private float balance;
	private AccountDetails accountDetails = null;
	public BankAccountImpl (float balance) throws RemoteException {
		super();
		this.balance = balance;
	}
    public void deposit (float amount) throws RemoteException {
		balance += amount;
	}
    public void withdraw (float amount) throws RemoteException {
		balance -= amount;
	}
    public float getBalance() throws RemoteException {
		return balance;
	}
	public void setAccountDetails (AccountDetails accountDetails) throws RemoteException {
		this.accountDetails = accountDetails;
	}
	public AccountDetails getAccountDetails() throws RemoteException {
		return accountDetails;
	}
}