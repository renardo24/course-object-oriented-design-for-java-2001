import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  BankCustomer.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class BankCustomer {
  public static void main(String[] args) {
    try {
	  System.setSecurityManager(new RMISecurityManager());
      BankAccount bankAccount = (BankAccount)Naming.lookup("rmi://localhost/BankAccountService");
      System.out.println("Balance is "+bankAccount.getBalance());
      bankAccount.withdraw(55.2F);
      System.out.println("After withdrawing 55.2: "+bankAccount.getBalance());
      bankAccount.deposit(11.0F);
      System.out.println("After depositing 11.0: "+bankAccount.getBalance());
      // Create AccountDetails and set it by passing object to bankAccount
      AccountDetails accountDetails = new AccountDetails("Joe Bloggs",1234);
      bankAccount.setAccountDetails(accountDetails);
      System.out.println("AcountDetails passed to accountDetails and set");
      AccountDetails retrievedDetails = bankAccount.getAccountDetails();
      System.out.println("Retrieved details: "+retrievedDetails.getName()+", "+retrievedDetails.getId());
    }
    catch (Exception ex) {
      System.out.println("Exception: "+ex);
    }
  }
}