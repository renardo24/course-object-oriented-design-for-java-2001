05 BankAccount-PassingObject
============================
Demonstrate passing objects by value to remote methods. This
demo is an extension of "04 BankAccount".

When an object implements the Remote interface, such objects
are passed by reference.  This is the case for BankAccount
class.

When an object implements the Serializable interface, it is
passed by value through a method.  This is the case for the
AccountDetails class.

If you remove the Serializable interface in the AccountDetails
class, the files will compile, but will fail at runtime.  Try
it and note the diagnostics for future reference.

Many useful API objects are already serialized - those in the
java.util package for instance.

Be careful - large objects passed by value will generate a
lot of network traffic.

Nic Nei (nnei@compuserve.com)
