import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  BankCustomer.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class BankCustomer {
  public static void main(String[] args) {
    try {
	  System.setSecurityManager(new RMISecurityManager());
      SavingsAccount savingsAccount = (SavingsAccount)Naming.lookup("rmi://localhost/BankAccountService");
      System.out.println("Balance is "+savingsAccount.getBalance());
      savingsAccount.withdraw(55.2F);
      System.out.println("After withdrawing 55.2: "+savingsAccount.getBalance());
      savingsAccount.deposit(11.0F);
      System.out.println("After depositing 11.0: "+savingsAccount.getBalance());
      savingsAccount.calculateInterest();
      System.out.println("Interest rate: "+savingsAccount.getInterestRate());
      System.out.println("After calculating interest "+savingsAccount.getBalance());
    }
    catch (Exception ex) {
      System.out.println("Exception: "+ex);
    }
  }
}