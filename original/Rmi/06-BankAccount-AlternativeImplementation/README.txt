04 BankAccount-AlternativeImplementation
========================================
This is an enhancement of "04 BankAccount".  In this demo,
we extends the BankAccount class to give us SavingsAccount.
This means that SavingsAccount cannot extend UnicastRemoteObject.

The alternative is to call UnicastRemoteObject.exportObject()
like so, in the constructor of SavingsAccountImpl:

    try {
        if (UnicastRemoteObject.unexportObject(this,true))
            System.out.println("Old SavingsAccount unexported successfully");
    }
    catch (NoSuchObjectException ex) {
        // do nothing
    }
    RemoteStub remoteStub = UnicastRemoteObject.exportObject(this);
    
The exportObject() operation must be the last statement in the
constructor.

Nic Nei (nnei@compuserve.com)
