import java.rmi.*;
/**
 *  SavingsAccount.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface SavingsAccount extends BankAccount, Remote {
	public float getInterestRate() throws RemoteException;
	public void calculateInterest() throws RemoteException;
}