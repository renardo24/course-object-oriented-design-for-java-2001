import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;
/**
 *  SavingsAccountImpl.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class SavingsAccountImpl extends BankAccountImpl implements SavingsAccount {
	private float interestRate;
	public SavingsAccountImpl (float balance, float interestRate) throws RemoteException {
		super(balance);
		this.interestRate = interestRate;
		try {
			if (UnicastRemoteObject.unexportObject(this,true))
				System.out.println("Old SavingsAccount unexported successfully");
		}
		catch (NoSuchObjectException ex) {
			// do nothing
		}
		RemoteStub remoteStub = UnicastRemoteObject.exportObject(this);
	}
    public float getInterestRate() throws RemoteException {
		return interestRate;
	}
    public void calculateInterest() throws RemoteException {
		float interest = (getBalance()/100) * interestRate;
		deposit(interest);
	}
}