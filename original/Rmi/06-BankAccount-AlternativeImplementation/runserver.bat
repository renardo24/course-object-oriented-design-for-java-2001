mkdir C:\Server
copy BankAccount.class C:\Server
copy SavingsAccount.class C:\Server
copy BankAccountServer.class C:\Server
copy SavingsAccountImpl.class C:\Server
copy SavingsAccountImpl_Skel.class C:\Server
copy SavingsAccountImpl_Stub.class C:\Server
copy BankAccountImpl.class C:\Server
copy BankAccountImpl_Skel.class C:\Server
copy BankAccountImpl_Stub.class C:\Server
copy POLICY C:\Server
cd C:\Server
java -Djava.rmi.server.codebase=file:/Server/ -Djava.security.policy=policy BankAccountServer