import java.rmi.*;
/**
 *  BankAccount.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface BankAccount extends Remote {
	public void deposit (double amount) throws RemoteException;
	public void withdraw (double amount) throws RemoteException;
	public double getBalance() throws RemoteException;
	public void addProxyAccount (ProxyAccount proxyAccount) throws RemoteException;
}