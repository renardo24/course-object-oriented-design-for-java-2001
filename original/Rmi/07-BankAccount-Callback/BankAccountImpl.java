import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;
/**
 *  BankAccountImpl.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class BankAccountImpl extends UnicastRemoteObject implements BankAccount {
	private ProxyAccount proxyAccount = null;
	private double balance;
	public BankAccountImpl(double balance) throws RemoteException {
		super();
		this.balance = balance;
	}
    public synchronized void deposit(double amount) throws RemoteException {
		balance += amount;
	}
	public synchronized void withdraw(double amount) throws RemoteException {
		balance -= amount;
		if (balance < 0)
			proxyAccount.update(balance);
	}
	public synchronized double getBalance() throws RemoteException {
		return balance;
	}
	public synchronized void addProxyAccount(ProxyAccount proxyAccount) throws RemoteException {
		this.proxyAccount = proxyAccount;
	}
}