import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;

/**
 *  BankAccountServer.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class BankAccountServer {
	public static void main(String args[]) {
		try {
			LocateRegistry.createRegistry(1099);
			System.setSecurityManager(new RMISecurityManager());
			BankAccount bankAccount = new BankAccountImpl(123.45);
			Naming.rebind("rmi://localhost:1099/BankAccountService", bankAccount);
			System.out.println("Bank is open for business...");
		}
		catch (Exception ex) {
			System.err.println("Exception: "+ex);
		}
	}
}