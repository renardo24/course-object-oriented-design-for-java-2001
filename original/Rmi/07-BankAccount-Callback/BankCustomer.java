import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.registry.*;

/**
 *  BankCustomer.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */

public class BankCustomer {

	public static void main(String[] args) throws Exception {
		ProxyAccount proxyAccount = new ProxyAccountImpl();
		System.out.println("Customer ready...");
		System.out.println("Balance: "+proxyAccount.getBalance());
		System.out.println("About to withdraw 1000...");
		proxyAccount.withdraw(1000.0);
  }
}