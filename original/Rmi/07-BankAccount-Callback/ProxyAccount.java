import java.rmi.*;
/**
 *  ProxyAccount.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public interface ProxyAccount extends Remote {
	public void deposit (double amount) throws RemoteException;
	public void withdraw (double amount) throws RemoteException;
	public double getBalance() throws RemoteException;
    public void update (double amount) throws RemoteException;
}