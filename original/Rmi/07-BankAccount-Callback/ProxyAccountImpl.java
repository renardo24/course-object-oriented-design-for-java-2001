import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;
/**
 *  ProxyAccountImpl.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ProxyAccountImpl extends UnicastRemoteObject implements ProxyAccount {
	private BankAccount bankAccount = null;
	public ProxyAccountImpl() throws RemoteException {
		super();
		System.setSecurityManager(new RMISecurityManager());
		try {
			bankAccount = (BankAccount)Naming.lookup("rmi://localhost/BankAccountService");
		}
		catch (Exception ex) {
			System.err.println(ex);
			System.exit(1);
		}
		bankAccount.addProxyAccount(this);
	}
    public synchronized void update (double amount) throws RemoteException {
		System.out.println("Account overdrawn: "+amount);
	}
	public void deposit (double amount) throws RemoteException {
		bankAccount.deposit(amount);
	}
	public void withdraw (double amount) throws RemoteException {
		bankAccount.withdraw(amount);
	}
	public double getBalance() throws RemoteException {
		return bankAccount.getBalance();
	}
}