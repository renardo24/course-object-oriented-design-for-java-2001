07 BankAccount-Callback
=======================
Demonstrate a server calling operations in the client.  This
demo is an extension of "04 BankAccount".

The BankAccount class now has an additional operation:

    public void addProxyAccount (ProxyAccount proxyAccount) throws RemoteException;
    
ProxyAccount is the client.  It is the "pretend" version of the
real account (BankAccount) on the server.  The BankCustomer
class creates the ProxyAccount.

The ProxyAccount's constructor looks up the server (BankAccount)
and "gives" itself to the server:

    public ProxyAccountImpl() throws RemoteException {
        super();
        System.setSecurityManager(new RMISecurityManager());
        try {
            bankAccount = (BankAccount)Naming.lookup("rmi://localhost/BankAccountService");
        }
        catch (Exception ex) {
            System.err.println(ex);
            System.exit(1);
        }
    }
    
Here's the sequence of events.

The server starts up (BankAccountServer).  It creates a BankAccount and
registers it with the registry.  The bank account is opened with an
initial balance of 123.45.

Then the client starts up (BankCustomer).  The customer creates a
ProxyAccount object.  The ProxyAccount constructor looks up the server
(the remote BankAccount object registered by the BankAccountServer
earlier).  Once located, the ProxAccount registers itself with the
remote BankAccount server object.  Now the BankAccount server object
has a handle to the ProxyAccount object and can call it back later.

The BankCustomer then checks the balance of the account (getBalance).
Look at ProxyAccountImpl.java - remember the ProxyAccount constructor
has located the BankAccount server object.  So ProxyAccount simply
invokes BankAccount server object getBalance() operation and relays
the result back to the BankCustomer.

The BankCustomer then invokes the withdraw() operation.  Like the
getBalance() operation, the ProxyAccount relays this call to the
remote BankAccount server object's withdraw() operation.  This
operation detects that the balance is negative and calls on the
update() operation of the ProxyAccount.  And that is the call back!

Nic Nei (nnei@compuserve.com)
