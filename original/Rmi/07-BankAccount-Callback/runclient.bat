mkdir C:\Client
copy BankCustomer.class C:\Client
copy ProxyAccount.class C:\Client
copy ProxyAccountImpl.class C:\Client
copy ProxyAccountImpl_Skel.class C:\Client
copy ProxyAccountImpl_Stub.class C:\Client
copy BankAccount.class C:\Client
copy POLICY C:\Client
cd C:\Client
java -Djava.rmi.server.codebase=file:/Client/ -Djava.security.policy=policy BankCustomer