/**
 *  ThreadController.java
 *  ThreadController creates and starts 3 threads running.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ThreadController {
	public static void main (String[] args) {
		ThreadTask threadTaskA = new ThreadTask("Thread-A");
		ThreadTask threadTaskB = new ThreadTask("           Thread-B");
		ThreadTask threadTaskC = new ThreadTask("                      Thread-C");

		threadTaskA.start();
		threadTaskB.start();
		threadTaskC.start();
	}
}