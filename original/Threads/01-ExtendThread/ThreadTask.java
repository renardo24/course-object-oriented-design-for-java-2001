/**
 *  ThreadTask.java
 *  Demonstrate the Thread class to implement multithreading
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ThreadTask extends Thread {
	private int count;
	private String threadName;

	public ThreadTask (String threadName) {
		this.threadName = threadName;
		count = 0;
	}

	public void run() {
		while (count < 10) {
			count++;
			System.out.println(threadName+": "+count);
			try {
				Thread.sleep(2000L);
			}
			catch (InterruptedException ex) {
				// ignore
			}
		}
	}
}