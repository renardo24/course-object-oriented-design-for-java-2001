/**
 *  ThreadController.java
 *  ThreadController creates and starts 3 threads running.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ThreadController {
	public static void main (String[] args) {
		ThreadableTask threadTaskA = new ThreadableTask("Thread-A");
		ThreadableTask threadTaskB = new ThreadableTask("           Thread-B");
		ThreadableTask threadTaskC = new ThreadableTask("                      Thread-C");

		threadTaskA.start();
		threadTaskB.start();
		threadTaskC.start();
	}
}