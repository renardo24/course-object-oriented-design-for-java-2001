/**
 *  ThreadableTask.java
 *  Demonstrate the Runnable interface to implement multithreading
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class ThreadableTask extends Thread {
	private int count;
	private String threadName;

	public ThreadableTask (String threadName) {
		this.threadName = threadName;
		count = 0;
	}

	public void run() {
		while (count < 10) {
			count++;
			System.out.println(threadName+": "+count);
			try {
				Thread.sleep(2000L);
			}
			catch (InterruptedException ex) {
				// ignore
			}
		}
	}
}