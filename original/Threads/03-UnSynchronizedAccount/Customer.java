/**
 *  Customer.java
 *  The Customer class represents a thread which periodically withdraws
 *  money from a bank account.  Several of these customers will withdraw
 *  from the account, so the Account class must synchronise its withdraw()
 *  method.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Customer extends Thread {
	private Account account;
	private String customerName;
	private long interval;

	public Customer (String customerName, Account account, int interval) {
		this.customerName = customerName;
		this.account = account;
		this.interval = interval * 100L;
	}

	public void run() {
		for (int count = 1; count <= 100; count++) {
			account.withdraw(1000);
			System.out.println("Balance: "+account.getBalance());
			try {
				Thread.sleep(interval);
			}
			catch (InterruptedException ex) {
				// ignore
			}
		}
	}
}