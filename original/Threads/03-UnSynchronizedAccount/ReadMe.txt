03 UnSynchronizedAccount
========================
Illustrate what happens with uncoordinate update to value of object.
The withdraw() operation of the the Account class is not synchronized:

	public void withdraw (double amount) {
		// Emulate a time consuming operation with a bit of processing
		double target;
		for (target = balance-amount; balance > target; balance -= 0.1)
			;
		balance = target;
	}
	
This should result in some lost withdrawals when one thread preempts
another.

To increase the chance of this happenning, we call withdraw() 1000 times.
We will have 10 customers (each in its own thread), each calling 
withdraw(1000) 100 times.  With an opening balance of 1000000, at the
end of the entire operation, the balance of the account should be 0.
But because withdraw() is not synchronized, some withdrawals will be
lost and the balance may not be 0.

Run the program several times.  Sometimes the end-balance may be 0, sometimes
greater than 0.

Nic Nei
(nnei@compuserve)