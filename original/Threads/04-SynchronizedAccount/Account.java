/**
 *  Account.java
 *  The Account class is "shared" by several customers.  Customers
 *  are multithreaded and can simultaneously withdraw() from the
 *  account.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class Account {
	private double balance;

	public Account (double balance) {
		this.balance = balance;
	}

	public synchronized void withdraw (double amount) {
		// Emulate a time consuming operation with a bit of processing
		double target;
		for (target = balance-amount; balance > target; balance -= 0.1)
			;
		balance = target;
	}

	public double getBalance() {
		return balance;
	}
}