/**
 *  AccountHolders.java
 *  AccountHolders represent several customers (each implemented as a
 *  Thread).  They will "simultaneously" withdraw money from a single
 *  account.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class AccountHolders {
	public static void main (String[] args) {
		Account account = new Account(1000000);

		System.out.println("Start balance: "+account.getBalance());

		Customer customerA = new Customer("CustomerA", account, 1);
		Customer customerB = new Customer("CustomerB", account, 1);
		Customer customerC = new Customer("CustomerC", account, 1);
		Customer customerD = new Customer("CustomerC", account, 1);
		Customer customerE = new Customer("CustomerC", account, 1);
		Customer customerF = new Customer("CustomerA", account, 1);
		Customer customerG = new Customer("CustomerB", account, 1);
		Customer customerH = new Customer("CustomerC", account, 1);
		Customer customerI = new Customer("CustomerC", account, 1);
		Customer customerJ = new Customer("CustomerC", account, 1);

		customerA.start();
		customerB.start();
		customerC.start();
		customerD.start();
		customerE.start();
		customerF.start();
		customerG.start();
		customerH.start();
		customerI.start();
		customerJ.start();

		Object object = new Object();
				try {
					object.wait();
				}
				catch (Exception ex) {
					// do nothing
		}
		System.out.println("Balance--- "+account.getBalance());
	}
}