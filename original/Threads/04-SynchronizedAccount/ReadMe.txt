04 4SynchronizedAccount
=======================
Illustrate what happens with coordinated update to the value of an object.
The withdraw() operation of the Account class is now synchronized:

	public synchronized void withdraw (double amount) {
		// Emulate a time consuming operation with a bit of processing
		double target;
		for (target = balance-amount; balance > target; balance -= 0.1)
			;
		balance = target;
	}

To increase the chance of losing some withdrawals, we call withdraw() 1000 times.
We will have 10 customers (each in its own thread), each calling 
withdraw(1000) 100 times.  With an opening balance of 1000000, at the
end of the entire operation, the balance of the account should be 0.

Run the program several times, just in case you a a 0 by chance.  Each
and every execution should result in a 0 end-balance.

Nic Nei
(nnei@compuserve)