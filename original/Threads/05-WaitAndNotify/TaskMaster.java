/**
 *  TaskMaster.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class TaskMaster extends Thread {

	private static TaskQueue taskQueue = new TaskQueue();
	public TaskMaster (String threadName) {
		super(threadName);
	}


	public void consumeJobs() {
		try {
			System.out.println("Waiting for job...");
			synchronized (taskQueue) {
				while (taskQueue.getJob() == null)
					taskQueue.wait();
			}
			System.out.println("Got the job...");
		}
		catch (InterruptedException ex) {
			// ignore
		}
	}


	public void produceJobs() {
		try {
			for (int count = 5; count >= 0; count--) {
				Thread.sleep(1000L);
				System.out.print(count+" ");
			}
			synchronized (taskQueue) {
				taskQueue.queueJob(new Job());
				taskQueue.notifyAll();
			}
			System.out.println(" Job queued");
		}
		catch (InterruptedException ex) {
			// ignore
		}
	}

	public void run() {
		if (getName().equals("producer"))
			produceJobs();
		if (getName().equals("consumer"))
			consumeJobs();
	}

	public static void main (String[] args) {
		TaskMaster consumer = new TaskMaster("consumer");
		TaskMaster producer = new TaskMaster("producer");
		consumer.start();
		producer.start();
	}
}