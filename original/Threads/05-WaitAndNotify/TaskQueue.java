/**
 *  TaskQueue.java
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class TaskQueue {
	private Job[] jobs;
	private int top;

	public TaskQueue() {
		jobs = new Job[5];
		top = 0;
	}

	public void queueJob (Job newJob) {
		if (top < jobs.length)
			jobs[top++] = newJob;
	}

	public Job getJob() {
		if (top == 0)
			return null;
		return jobs[--top];
	}
}