/**
 *  MyObject.java
 *	Demonstrate deadlocks in Java.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyObject {
	public synchronized void useA (MyObject a) {
		System.out.println("Enter a.methodA()");
		keepBusy(5);
		a.methodA();
		System.out.println("Exit a.methodA()");
	}

	public synchronized void useB (MyObject b) {
		System.out.println("Enter b.methodB()");
		keepBusy(5);
		b.methodB();
		System.out.println("Exit b.methodB()");
	}

	public synchronized void methodA() {
		System.out.println("Enter methodA()");
		keepBusy(5);
		System.out.println("Exit methodA()");
	}

	public synchronized void methodB() {
		System.out.println("Enter methodB()");
		keepBusy(5);
		System.out.println("Exit methodB()");
	}

	private void keepBusy (long seconds) {
		try {
			Thread.sleep(seconds * 1000L);
		}
		catch (InterruptedException ex) {
			//ignore
		}
	}
}