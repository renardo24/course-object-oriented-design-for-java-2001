/**
 *  MyThreads.java
 *	Demonstrate deadlocks in Java.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyThreads extends Thread {
	private static MyObject a = new MyObject();
	private static MyObject b = new MyObject();

	public MyThreads (String threadName) {
		super(threadName);
	}

	public void run() {
		if (getName().equals("thread1"))
			a.useB(b);
		if (getName().equals("thread2"))
			b.useA(a);
	}

	public static void main (String[] args) {
		MyThreads thread1 = new MyThreads("thread1");
		MyThreads thread2 = new MyThreads("thread2");

		thread1.start();
		thread2.start();
	}
}