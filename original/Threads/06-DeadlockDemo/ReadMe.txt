06 DeadlockDemo
===============
Demonstrate a deadlock.  We have a object (MyObject) that has 4 methods:

	useA()
	useB()
	methodA()
	methodB()

useA() will be given reference to object A and then object A's methodA() invoked.
useB() will be given reference to object B and then object B's methodB() invoked.
	
MyThread creates 2 threads.  Each thread will do the following:

	thread1 invokes a.useB(b)
	thread2 invokes b.useA(a)
	
The result is a deadlock because object a will wait for object b to release its
lock so that it can use b.  Meanwhile b will wait for object a to releases its
lock so that it can use a.

Nic Nei
(nnei@compuserve)