import java.util.*;
/**
 *  MyCollection.java
 *	Demonstrate partial synchronization.  Avoid synchronizing whole methods
 *	in order to improve liveness of objects.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyCollection {
	private Collection data;

	public MyCollection() {
		data = new Vector();
	}
	/**
	 *  Append append to the data vector, then return an array representation
	 *	of data.  Notice that the method is not synchronized, only that part
	 *	of the method that does the appending.
	 */
	public Object[] getCombinedData (Object[] append) {
		Object[] combo = null;
		int size = 0, extra = append.length;

		synchronized (data) {
			size = data.size();
			combo = new Object[size + extra];
			for (int ix = 0; ix<append.length; ix++)
				data.add(append[ix]);

			combo = data.toArray(combo);
		}
		System.arraycopy(append,0,combo,size,extra);
		return combo;
	}
	/**
	 *	Return an array representation of data vector.
	 */
	public Object[] toArray() {
		Object[] array = new Object[data.size()];

		Iterator iter;
		int ix;
		synchronized (data) {
			for (ix = 0, iter=data.iterator(); iter.hasNext(); ix++)
				array[ix] = iter.next();
		}

		return array;
	}
}