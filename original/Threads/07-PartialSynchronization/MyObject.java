/**
 *  MyObject.java
 *	Demonstrate partial synchronization.  Avoid synchronizing whole methods
 *	in order to improve liveness of objects.
 *
 *	MyObject provides the target of the MyCollection class.  MyObject is
 *	essentially just a factory which creates unique instances of objects.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyObject {
	private static int nextId = 1;
	private int myId;

	private MyObject() {
		myId = nextId++;
	}

	public static MyObject create() {
		return new MyObject();
	}

	public String toString() {
		return ""+myId;
	}

	public int getMyId() {
		return myId;
	}
}