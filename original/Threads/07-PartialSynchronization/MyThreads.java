import java.util.*;
/**
 *  MyThreads.java
 *	Demonstrate partial synchronization.  Avoid synchronizing whole methods
 *	in order to improve liveness of objects. We create several threads here.
 *	Each thread will append a small array of MyObjects to the collection.
 *	If they are synchronized properly, the collection will have a sequential
 *	collection of MyObjects with unique numbers.  At the end of the program
 *	we wait for all the threads to complete then check that the collection
 *	is sequential and contains unique objects.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyThreads extends Thread {
	private static MyCollection collection = new MyCollection();

	public MyThreads (String threadName) {
		super(threadName);
	}
	/**
	 *  Each thread will start in this method.  Here we generate a random number
	 *	of MyObjects to create in an array (extraArray), and a random number of
	 *	seconds to delay (to emulate a method taking some time to execute - this
	 *	maximises chance of threads pre-empting each other).  Then we call on
	 *	getCombinedData() to append extraArray to the end of the collection.
	 */
	public void run() {
		int count = (int)(Math.random()*10);
		int delay = (int)(Math.random()*10);

		MyObject[] extraArray = new MyObject[count];
		for (int ix=0; ix<count; ix++)
			extraArray[ix] = MyObject.create();

		Object[] arrayRepresentation = collection.getCombinedData(extraArray);
		keepBusy(delay);
	}
	/**
	 *	Emulate some delay in operation.
	 */
	private void keepBusy (long seconds) {
		try {
			Thread.sleep(seconds * 200L);
		}
		catch (InterruptedException ex) {
			//ignore
		}
	}
	/**
	 *	Create a controlThread to coordinate things.  We then create 25 threads.
	 *	Each thread will independently append an array of MyObjects to the
	 *	collection.  If all the threads are synchronized correctly, this collection
	 *	will contain a unique and sequential collection of MyObjects.  controlThread
	 *	will wait for all the thread to complete, then verify this array.
	 */
	public static void main (String[] args) throws InterruptedException {
		MyThreads threadControl = new MyThreads("control");
		for (int count=0; count < 25; count++) {
			MyThreads oneThread = new MyThreads("thread"+count);
			oneThread.start();
		}
		// threadControl and MyThreads each count for one thread.
		while (threadControl.activeCount() != 2) {
			System.out.println("Waiting for all threads to finish");
			threadControl.keepBusy(5);
		}

		Object[] array = collection.toArray();
		System.out.println("All threads finished. "+array.length+" elements created");
		for (int ix=1; ix<=array.length; ix++) {
			MyObject myObject = (MyObject)(array[ix-1]);
			if (myObject.getMyId() != ix) {
				System.out.println("We have a problem: "+ix+" -- "+myObject.getMyId());
				//System.exit(1);
			}
		}
		System.out.println("Collection is OK!");
	}
}