07 PartialSynchronization
=========================
Demonstrate the use of partial synhronization to maximise liveness.
In MyCollection, the getCombinedData() and toArray() operations
are not synchronized on the method.  Instead, we synchronized where
necessary..  This is getCombinedData():

	public Object[] getCombinedData (Object[] append) {
	    Object[] combo = null;
	    int size = 0, extra = append.length;

	    synchronized (data) {
		size = data.size();
		combo = new Object[size + extra];
		for (int ix = 0; ix<append.length; ix++)
		    data.add(append[ix]);

		combo = data.toArray(combo);
	    }
	    System.arraycopy(append,0,combo,size,extra);
	    return combo;
	}
	
And this is toArray():

	public Object[] toArray() {
	    Object[] array = new Object[data.size()];

	    Iterator iter;
	    int ix;
	    synchronized (data) {
		for (ix = 0, iter=data.iterator(); iter.hasNext(); ix++)
		    array[ix] = iter.next();
	    }

	    return array;
	}
	
We use 25 simultaneous threads to verify these 2 operations.  Each thread
will independently append an array of MyObjects to the collection which
will be implemented as a Vector in MyCollection.  Each MyObject created
will be uniquely identified using a serial number that sequentially 
increments for each MyObject created.  Additionally, the size of the array
of MyObjects appended by each thread to the collection will be random in
size, and each thread will idle for a random number of milliseconds.  All
this is done in order to maximise the chances of pre-emption by other
threads and corruption of the collection.

Our program creates a control thread (controlThread) which will wait for
the 25 threads to finish and then validates this collection.  Each MyObject
in the collection will be sequentially numbered from 1.  The next MyObject
will be 2, and the next one 3, and so on.

Nic Nei
(nnei@compuserve)