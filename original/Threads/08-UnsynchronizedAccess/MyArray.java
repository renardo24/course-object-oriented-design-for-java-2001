/**
 *  MyObject.java
 *	Demonstrate unsynchronized access.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyArray{
	private int size, increment = 5;
	private Object[] array = new Object[increment];

	public int getSize() {
		return size;
	}

	public synchronized void append(Object object) {
		if(size >= array.length){
			Object[] old = array;
			array = new Object[size + increment];
			System.arraycopy(old, 0, array, 0, size);
		}
		array[size++] = object;
	}

	public synchronized void removeLast() {
		array[--size] = null;
	}

	public Object[] getArray() {
		Object[] result = new Object[size];
		for (int ix=0; ix<size; ix++)
			result[ix] = array[ix];
		return result;
	}
}
