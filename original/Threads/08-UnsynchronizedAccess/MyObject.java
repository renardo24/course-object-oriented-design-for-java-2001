/**
 *  MyObject.java
 *	Demonstrate unsynchronized access.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyObject {
	private static int nextId = 1;
	private int myId;

	private MyObject() {
		myId = nextId++;
	}

	public static MyObject create() {
		return new MyObject();
	}

	public String toString() {
		return ""+myId;
	}

	public int getMyId() {
		return myId;
	}
}