/**
 *  MyThreads.java
 *	Demonstrate unsynchronized access.
 *
 *  Nic Nei (nnei@compuserve.com)
 */
public class MyThreads extends Thread {
	private static MyArray myArray = new MyArray();

	public MyThreads (String threadName) {
		super(threadName);
	}

	public void run() {
		int threadNumber = Integer.parseInt(getName());
		int count = (int)(Math.random()*20);
		int delay = (int)(Math.random()*10);

		if (threadNumber % 2 == 0) {
			System.out.println("append "+count+" Objects");
			for (int ix = 0; ix<count; ix++) {
				myArray.append(MyObject.create());
				keepBusy(delay);
			}
		} else {
			System.out.println("Removing "+count+" Objects");
			for (int ix = 0; ix<count; ix++) {
				if (myArray.getSize() == 0)
					continue;
				myArray.removeLast();
				keepBusy(delay);
			}
		}
	}

	/**
	 *	Emulate some delay in operation.
	 */
	private void keepBusy (long seconds) {
		try {
			Thread.sleep(seconds * 200L);
		}
		catch (InterruptedException ex) {
			//ignore
		}
	}

	public static void main (String[] args) {
		MyThreads threadControl = new MyThreads("control");
		for (int count=0; count < 25; count++) {
			MyThreads oneThread = new MyThreads(""+count);
			oneThread.start();
		}
		// threadControl and MyThreads each count for one thread.
		while (threadControl.activeCount() != 2) {
			System.out.println("Waiting for all threads to finish");
			threadControl.keepBusy(8);
		}
		System.out.println("Size of MyArray: "+myArray.getSize());
		Object[] array = myArray.getArray();
		System.out.println("array.length: "+array.length);
		for (int ix=0; ix<array.length; ix++)
			System.out.println(ix+": "+((MyObject)array[ix]).toString());
	}
}