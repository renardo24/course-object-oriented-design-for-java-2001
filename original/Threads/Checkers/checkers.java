// file: checkers.java
// Applet to demonstrate the flicker problem when the default update()
// method clears the applet screen before redrawing it.

// A modification of this applet, checkersnoflicker.java fixes this
// problem.

import java.awt.*;
import java.applet.*;

public class checkers extends Applet implements Runnable {
	int xpos;
	Thread thread;

	public void start(){
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	public void stop(){
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}
	public void run(){
		setBackground(Color.blue);
		while(true) {
			for (xpos=5; xpos<=105; xpos+=4) {
				repaint();
				try {
					thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
			}
			for (xpos=105; xpos>=5; xpos-=4) {
				repaint();
				try {
					thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
			}
		}
	}

	public void paint (Graphics g) {
		// draw background
		g.setColor(Color.black);
		g.fillRect(0,0,100,100);
		g.setColor(Color.white);
		g.fillRect(101,0,100,100);

		// draw checker
		g.setColor(Color.red);
		g.fillOval(xpos,5,90,90);
	}
}