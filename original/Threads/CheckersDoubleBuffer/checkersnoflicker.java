// file: checkersnoflicker.java

// Demonstrate the use of double-buffering to remove flickering in
// animated graphics.  This involves creating a second surface that
// if off-screen and paint to that surface each of the elements you
// want to display at one time.  When that has been done, draw the
// whole off-screen surface at once onto the actual applet.  Because
// all the work goes on behind the scenes, there's no opportunity
// for interim parts of the drawing process to appear accidentally
// and disrupt the smoothness of the animation.  4 steps are involved:

// 1.  Add instance variables to hold image and graphics context for
//     the off-screen buffer.
// 2.  Create an image and Graphics context object in the init() method.
// 3.  Do all applet painting to off-screen buffer, not the applet's
//     drawing surface.
// 4.  At the end of your paint method, draw the off-screen buffer to
//     the real screen.

// The createImage() method creates an off-screen drawable image, 
// which can be used for double buffering. 

// The getGraphics() method Creates a graphics context for drawing to an 
// off-screen image. This method can only be called for off-screen images, 
// which are created with the createImage() method with two integer arguments.

import java.awt.*;
import java.applet.*;

public class checkersnoflicker extends Applet implements Runnable {
	int xpos;
	Thread thread;
	Image offscreenImage;
	Graphics offscreenGraphics;

	public void init(){
		//offscreenImage = createImage(size().width,size().height);
		offscreenImage = createImage(200,100);
		offscreenGraphics = offscreenImage.getGraphics();
	}
	public void start(){
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	public void stop(){
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}
	public void run(){
		setBackground(Color.blue);
		while(true) {
			for (xpos=5; xpos<=105; xpos+=4) {
				repaint();
				try {
					thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
			}
			for (xpos=105; xpos>=5; xpos-=4) {
				repaint();
				try {
					thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
			}
		}
	}
	public void update (Graphics g) {
		paint(g);
	}
	public void paint (Graphics g) {
		// draw background
		offscreenGraphics.setColor(Color.black);
		offscreenGraphics.fillRect(0,0,100,100);
		offscreenGraphics.setColor(Color.white);
		offscreenGraphics.fillRect(101,0,100,100);

		// draw checker
		offscreenGraphics.setColor(Color.red);
		offscreenGraphics.fillOval(xpos,5,90,90);

		g.drawImage(offscreenImage,0,0,this);
	}
}