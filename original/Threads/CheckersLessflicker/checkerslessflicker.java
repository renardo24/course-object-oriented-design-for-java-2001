// file: checkerslessflicker.java
// Applet to demonstrate overriding the update() method to reduce flickering
// when animating graphics.  The integer variables ux1 and ux2 specify the
// left and right bounds of the clip-rectangle that needs to be updated.
// Although this solution does not eliminate all flicker in the animation,
// it reduces it a great deal.

import java.awt.*;
import java.applet.*;

public class checkerslessflicker extends Applet implements Runnable {
	int xpos;
	int ux1, ux2;
	Thread thread;

	public void start(){
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	public void stop(){
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}
	public void run(){
		setBackground(Color.blue);
		while(true) {
			for (xpos=5; xpos<=105; xpos+=4) {
				ux2 = xpos+90;
				repaint();
				try {
					thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
				if (ux1 == 0)
					ux1 = xpos;
			}
			for (xpos=105; xpos>=5; xpos-=4) {
				ux1 = xpos;
				repaint();
				try {
					thread.sleep(100);
				}
				catch (InterruptedException ex) {
				}
				if (ux2 == 0)
					ux2 = xpos+90;
			}
		}
	}
	public void update (Graphics g) {
		g.clipRect(ux1,5,ux2-ux1,95);
		paint(g);
	}
	public void paint (Graphics g) {
		// draw background
		g.setColor(Color.black);
		g.fillRect(0,0,100,100);
		g.setColor(Color.white);
		g.fillRect(101,0,100,100);

		// draw checker
		g.setColor(Color.red);
		g.fillOval(xpos,5,90,90);

		// reset the drawing area
		ux1 = ux2 = 0;
	}
}