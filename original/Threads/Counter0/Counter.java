import java.applet.*;
import java.awt.*;

public class Counter extends java.applet.Applet {
    private int count;
    
    public void paint (Graphics g) {
        g.setFont(new Font("TimesRoman",Font.BOLD,36));
        while (true) {
            count++;
            g.drawString ("Count = " + count, 50,50);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {}
            g.setColor(getBackground());
            g.fillRect(0,0,getSize().width,getSize().height);
            g.setColor(Color.black);
        }
    }
}