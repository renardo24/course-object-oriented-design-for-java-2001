import java.applet.*;
import java.awt.*;

public class Counter extends Applet implements Runnable {
    private Thread thread;
    private int count;
    Font myfont = new Font("TimesRoman",Font.BOLD,36);
 
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }
    public void stop() {
        thread.stop();
        thread = null;
    }
    public void run() {
        while (true) {
            count++;
            repaint();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {}
        }
    }
    public void paint (Graphics g) {
        g.setFont(myfont);
        g.drawString("Count = " + count,50,50);
    }
}