import java.awt.*;

public class Counter implements Runnable {
    private TextField txt;
    private int count;
    
    public Counter (TextField t) {
        txt = t;
    }
    public void run() {
        while (true) {
            count++;
            txt.setText("Counter = "+count);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e){}
        }
    }
}