import java.applet.*;
import java.awt.*;

public class CounterTest extends Applet {
    private TextField txt1, txt2;
    private Counter count1, count2;
    private Thread thread1, thread2;
    
    public void init() {
        setFont(new Font("TimesRoman",Font.BOLD,24));
        txt1 = new TextField(12);
        txt2 = new TextField(12);
        add(txt1);
        add(txt2);
    }
    public void start() {
        count1 = new Counter(txt1);
        count2 = new Counter(txt2);
        thread1 = new Thread(count1);
        thread2 = new Thread(count2);
        thread1.start();
        thread2.start();
    }
    public void stop() {
        thread1.stop();
        thread2.stop();
        thread1 = null;
        thread2 = null;
    }
}        