ProducerConsumer-Unsynchronized
===============================
Demonstrate what happens when threads are not synchronized.  In this
demo, producer puts an integer into CubbyHole while Consumer thread
reads from it.  If both threads are synchronized, the numbers displayed
will be sequential and unique - i.e. producer puts 1 in, consumer reads 1,
then producer puts 2 in, consumer reads 2, and so on.  Instead we get
repititions (when consumer is too fast) or missing numbers (when consumer
is too slow):

Consumer #1 got: 3
Producer #1 put: 4
Producer #1 put: 5
Consumer #1 got: 5
...

This demo is taken from java.sun.com

Nic Nei
(nnei@compuserve.com)