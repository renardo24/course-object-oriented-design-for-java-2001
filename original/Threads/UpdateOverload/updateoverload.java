// file: updateoverload.java
// This applet displays a string which swirls into different colours
// every 20 milliseconds.  If you compile this with the update() method
// commented out, you will notice the colours flickering because the
// default update() clears the screen, then calls paint() to draw the text.

// Since our string only changes colour, and not its shape or form, there
// is no need to clear the screen.  By overriding the default update()
// method (make sure the update() method below is not commented out), to
// just paint() the string, the flicker is removed.

import java.awt.*;
import java.applet.*;

public class updateoverload extends Applet implements Runnable {
	Font myfont = new Font("TimesRoman",Font.BOLD,48);
	Color mycolours[] = new Color[50];
	Thread thread;

	public void start(){
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	public void stop(){
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}
	public void update (Graphics g) {
		paint(g);
	}
	public void run(){
		// set the colour array
		float col = 0F;
		for (int ind=0; ind<mycolours.length; ind++) {
			mycolours[ind] = Color.getHSBColor(col,1.0F,1.0F);
			col += 0.02F;
		}
		// cycle through the colours
		int ind = 0;
		while (true) {
			setForeground(mycolours[ind]);
			repaint();
			ind++;
			try {
				thread.sleep(20);
			}
			catch (InterruptedException ex) {
			}
			ind = mycolours.length == ind ? 0 : ind;
		}
	}
	public void paint (Graphics g) {
		g.setFont(myfont);
		g.drawString("Look to the colours!",20,60);
	}
}