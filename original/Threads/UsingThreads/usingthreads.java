// file: usingthreads.java
// This applet uses simple animation to display a digital clock and 
// updates it every second.

// 1.  The signature of the applet class must include "implements Runnable".
// 2.  Include an instance variable to hold your apple's thread.
// 3.  Override the start() method to spawn a thread and start it running.
// 4.  Override the run() method to contain the applet code.
// 5.  Override the stop() method to suspend execution when the user leaves
//     the page containing the applet.

// The following applet will occasionally flicker because Java paints and
// repaints each frame.  The repaint() method in run() calls update() which
// clears the screen (by filling it with the current background) and then
// calls paint() to display the digital clock.  This causes flicker.

// The yellow rectangle is there to help you observe the flicker.

// If we override the update() method by only redrawing the digital clock,
// then the flicker is reduced.  Uncomment the update() method below to
// see this fix.  The clipRect() method tells java only to redraw that
// rectangular region.  The yellow box now no longer flickers.

import java.awt.*;
import java.applet.*;
import java.util.Date;

public class usingthreads extends Applet implements Runnable{

	Font myfont = new Font("TimesRoman",Font.BOLD,24);
	FontMetrics metrics;
	String string;
	Date mydate;
	Thread thread;  // this is the instance variable for the apple's thread

	public void start(){
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	public void stop(){
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}
	public void run(){
		while (true) {
			mydate = new Date();
			repaint();
			try {
				Thread.sleep(1000);
			}
			catch (InterruptedException e) {
			}
		}
	}/*
	public void update(Graphics g) {
		if (string == null) {// first time: just paint the whole thing
			paint(g);
			return;
		}

		g.clipRect(8,28,metrics.stringWidth(string)+2,metrics.getHeight()+3);
		Color back = getBackground();
		g.setColor(back);

		g.fillRect(8,28,metrics.stringWidth(string)+2,metrics.getHeight()+3);
		g.setColor(Color.black);
		paint(g);
	}*/
	public void paint (Graphics g) {
		g.setColor(Color.yellow);
		g.fillRect(100,100,300,100);
		g.setColor(Color.blue);
		g.fillOval(230,130,40,40);
		g.setColor(Color.black);

		g.setFont(myfont);
		metrics = getFontMetrics(myfont);
		string = mydate.toString();
		g.drawRect(5,25,metrics.stringWidth(string)+10,35);
		g.drawString(string,10,50);
	}
}